<?php

/**
 * Схема:
 * extends, указывает какой подкласс наследует аттрибут, имеет приоритет над автоматическим поиском подкласса
 *
 * У большинства значений аттрибутов может быть
 * человеко-читаемое значение (ЧЧЗ) (т.е. значение из справочника, из другой сопряженной таблицы и т.д.)
 */
defined('SDB_DIR') || define('SDB_DIR', realpath(__DIR__) . DIRECTORY_SEPARATOR);
defined('SDB_PHP_EXT') || define('SDB_PHP_EXT', '.php');

class SDB_Attribute
{


    protected $id;
    /**
     * имя аттрибута
     *
     * @var string
     */
    protected $name;

    /**
     * @var
     */
    public $components;
    protected $value;
    protected $human_value;
    protected $old_value;
    protected $modified;

    protected $aScheme;
    protected $aCompScheme;

    /**
     * @var SDB_Entity
     */
    private $entity;


    public function __construct($attr_name, $aScheme)
    {
        $this->aScheme = $aScheme;
        $this->name = $attr_name;
    }

    public static function factory($attr_name, &$aScheme, SDB_Entity &$entity = null)
    {


        /**
         * todo: make lazy initialization
         */

        if (is_object($aScheme)) {
            $obj =& $aScheme;
        } else {
            if (!is_array($aScheme)) {
                $tmp = SDB_Entity::get_schema($aScheme);
                if (!isset($tmp[$attr_name])) {
                    throw new Exception('No attr "' . $attr_name . '" in Scheme "' . $aScheme . '"');
                } else {
                    $aScheme = $tmp[$attr_name];
                    $tmp = null;
                }

            }

            if (!empty($aScheme['extends'])) {
                $classname = 'SDB_Attribute_' . $aScheme['extends'];
            } else {
                $classname = 'SDB_Attribute_' . $attr_name;
            }


            /**
             * Нахождение класса аттрибута
             * Если задано внешнее описание класса файла, то доверяемся автозагрузчику класса
             * Если задано исключение наследования, то проверяется только класс аттрибута по имени,
             * если последнего нет, то наследование происходит от первого найденного базового класса аттрибутов.
             */
            $segments = explode(DIRECTORY_SEPARATOR, str_replace('_', DIRECTORY_SEPARATOR, $classname));
            if (empty($aScheme['autoload'])) {
                if (empty($aScheme['extends'])) {
                    if (!is_file(SDB_DIR . implode(DIRECTORY_SEPARATOR, array_slice($segments, 1)) . SDB_PHP_EXT)) {
                        $segments = array('SDB', 'Attribute');
                    }
                } else {
                    while (!is_file(SDB_DIR . implode(DIRECTORY_SEPARATOR, array_slice($segments, 1)) . SDB_PHP_EXT) && array_pop($segments)) ;
                }
                $classname = implode('_', $segments);
                include_once(SDB_DIR . implode(DIRECTORY_SEPARATOR, array_slice($segments, 1)) . SDB_PHP_EXT);
            } else {
                include_once(SDB_Entity::get_autoload_dir() . implode(DIRECTORY_SEPARATOR, $segments) . SDB_PHP_EXT);
            }


            /**
             * @var $obj SDB_Attribute
             */
            $obj = new $classname($attr_name, $aScheme);
        }
        if ($obj->get_type() === 'external') {
            $ext = $entity->get_external($attr_name);
            $obj->set_entity($ext);
        } else {
            $obj->set_entity($entity);
        }
        return $obj;
    }

    private function set_entity(&$entity)
    {
        $this->entity =& $entity;
        return $this;
    }

    /**
     * @param bool $top
     *
     * @return SDB_Entity
     */
    protected function &get_entity($top = false)
    {
        if ($top) {
            return $this->entity->get_top_parent();
        }
        return $this->entity;
    }

    /**
     * Устанавливет значение аттрибута по-умолчанию
     *
     * @return $this
     */
    public function set_default()
    {
        $this->human_value = NULL;
        $this->unused_ids = [];
        $this->id = null;

        if ($this->is_multiple()) {

            if (isset($this->aScheme['default'])) {
                $this->set_raw_value($this->aScheme['default']);
            } else {
                $this->value = [];
            }
        } else {
            if (isset($this->aScheme['default'])) {
                $this->set_raw_value($this->aScheme['default']);
            } else {
                $this->set_raw_value(NULL);
            }
        }
        return $this;
    }

    /**
     * Возвращает значение по-умолчанию
     *
     * @return bool|null
     */
    public function get_default()
    {
        return isset($this->aScheme['default']) ? $this->aScheme['default'] : null;
    }

    /**
     * Возвращает тип аттрибута (int,bool,text,decimal,varchar);
     *
     * @return string
     */
    public function get_type()
    {
        return $this->aScheme['type'];
    }

    public function set_default_value($value)
    {
        $this->aScheme['default'] = $value;
        return $this;
    }


    public function add_component($name, &$scheme)
    {
        $this->aCompScheme[$name] =& $scheme;
    }

    public function get_component($name)
    {
        return $this->aCompScheme[$name];
    }

    /**
     * @return array | SDB_Attribute[]
     */
    public function get_components()
    {
        if (!isset($this->aCompScheme)) {
            return [];
        }
        return $this->aCompScheme;

    }

    public function get_components_names($deep = false)
    {
        if (!isset($this->aCompScheme)) {
            return [];
        }
        $cmps = array_keys($this->aCompScheme);
        if ($deep) {
            foreach ($this->aCompScheme as $cmp) {
                $cmps = array_merge($cmps, $cmp->get_components_names(true));
            }
        }
        return $cmps;
    }

    public function is_multiple()
    {
        return !empty($this->aScheme['multiple']) || !empty($this->aScheme['is_multiple']);
    }

    public function is_multiple_set()
    {
        return $this->is_multiple() && substr($this->get_name(), -4) === '_set';
    }

    public function is_range()
    {
        return is_a($this, 'SDB_Attribute_range');
    }


    public function is_list()
    {
        return isset($this->aScheme['values']) && !in_array($this->get_type(), array('function'), true);
    }

    public function is_source()
    {
        return isset($this->aScheme['source']);
    }

    public function is_required()
    {
        return !empty($this->aScheme['required']);
    }

    public function is_keep()
    {
        return !empty($this->aScheme['keep']);
    }

    public function is_refbook()
    {
        return !empty($this->aScheme['extends']) && $this->aScheme['extends'] === 'refbook';
    }

    public function is_composite()
    {
        return $this->get_type() === 'composite';
    }

    public function is_modified()
    {
        return $this->modified;
    }

    public function is_hidden()
    {
        return !empty($this->aScheme['hide']);
    }

    public function is_zero_possible()
    {
        return !empty($this->aScheme['zero']);
    }

    public function allow_export()
    {
        return !isset($this->aScheme['export']) or $this->aScheme['export'];
    }

    public function allow_log()
    {
        return !isset($this->aScheme['log']) or $this->aScheme['log'];
    }

    public function is_visible($role)
    {
        $show = false;

        if ($this->is_hidden()) {
            if (in_array('*', $this->aScheme['hide'], true)) {
                return in_array($role, isset($this->aScheme['show']) ? $this->aScheme['show'] : [], true);
            }
            if (in_array($role, $this->aScheme['hide'], true)) {
                return false;
            }
        }
        return true;
    }

    public function get_original_set_name()
    {
        return substr($this->get_name(), 0, -4);
    }

    /**
     *  Проверка существенности значения (для сохранения в базе)
     */
    protected function is_empty_value($value)
    {
        return is_array($value) ? empty($value) : (is_null($value) or trim($value) === '' or (!$this->is_zero_possible() && in_array($this->get_type(), array('decimal', 'int'), true) && (float)$value === (float)0));
    }

    /**
     * Проверяет наличие значения аттрибута
     *
     * @param type $deep Проверять в том числе у компонентов
     */
    public function has_value($deep = true)
    {


        if ($deep && $this->is_composite()) {
            foreach ($this->get_components() as $ext_attr_obj) {
                if (!$ext_attr_obj->has_value(true)) {
                    return false;
                }
            }
            return true;
        }

        return !$this->is_empty_value($this->get_raw_value());
    }

    public $unused_ids = [];

    public function set_id($id)
    {
        if ($this->is_multiple()) {
            if (is_array($id)) {
                $this->id = array_values($id);
            } else {
                $this->id[] = $id;
            }
        } else {

            if ($this->id && $id != $this->id) {
                $this->unused_ids[] = $this->id;
            }
            $this->id = $id;
        }
        return $this;
    }

    /**
     * @param null $num
     *
     * @return int|array
     */
    public function get_id($num = null)
    {
        if ($this->is_multiple()) {
            return is_null($num) ? $this->id : (isset($this->id[$num]) ? $this->id[$num] : null);
        }
        return $this->id;
    }


    public function get_title()
    {
        return preg_replace('~кв\.м\.?~', 'м²', $this->aScheme['title']);
    }

    public function get_suffix()
    {
        return isset($this->aScheme['suffix']) ? preg_replace('~кв\.м\.?~', 'м²', $this->aScheme['suffix']) : '';
    }

    /**
     * генерировать ли специальное человекочитаемое значение
     * @return bool
     */
    public function use_human(){
        return !empty($this->aScheme['use_human']) || in_array($this->get_type(), [SDB_Entity::TYPE_INT,SDB_Entity::TYPE_DECIMAL]);
    }

    public function set_old_value($value)
    {
        $this->old_value = $value;
        return $this;
    }

    /**
     * Обновляет статус измененности
     *
     * @param bool $forceModify - если есть изменения, учитывать ли их
     *
     * @return $this
     */
    protected function update_modified($forceModify = false)
    {
        if ($this->old_value != $this->value || ($this->is_zero_possible() && $this->old_value !== $this->value)) {
            $this->modified = $this->modified || (bool)$forceModify;
        }
        return $this;
    }

    /**
     * Установка свойства аттрибута
     *
     * @param      $value
     * @param bool $modify учитывать ли эту установку в изменениях аттрибута
     * @param null $index порядковый элемент в массиве (для аттрибутов со множеством значений)
     *
     * @return $this
     */
    public function set_value($value, $modify = true, $index = null)
    {


        /*
         * Нужно именно изначальное значение (загруженное из базы)
         * для последующего, корректного удаления старых значений
         */
        $this->set_old_value($this->value);


        if ($this->is_multiple()) {
            if (is_array($value)) {
                $this->value = $value;
            } else {
                $value = $this->_get_normalized_value($value);


                if (!is_null($index)) {
                    $this->value[$index] = $value;
                } else {
                    $this->value[] = $value;
                }
            }
            if ($this->is_list() && !$this->is_range()) {
                $this->value = array_unique($this->value);
            }
        } else {
            if (is_array($value)) $value = reset($value);

            $this->value = $this->_get_normalized_value($value);

        }

        $this->update_modified($modify);

        return $this;
    }

    /**
     * В случае доверия к источнику (пример: загрузка из базы),
     * для ускорения обработки массива информации,
     * при отсутствии необходимости дальнейшего обновления значений в базе
     *
     * @param      $value
     * @param bool $modify
     * @param null $index
     *
     * @return $this
     */
    public function set_raw_value($value)
    {
        /*
         * Нужно именно изначальное значение (загруженное из базы)
         * для последующего, корректного удаления старых значений
         */
        $this->old_value = $this->value;

        if ($this->is_multiple()) {
            if (is_array($value)) {
                $this->value = $value;
            } else {
                $this->value[] = $value;
            }
        } else {
            $this->value = is_array($value) ? reset($value) : $value;
        }

        return $this;
    }

    public function add_value($value, $modify = true, $index = null)
    {
        if (!$this->is_multiple()) {
            return $this->set_value($value, $modify, $index);
        }
        $value = $this->_get_normalized_value($value);
        if (!is_array($this->value)) {
            $this->value = [];
        }

        if (!is_null($index)) {
            $this->value[$index] = $value;
        } elseif (!$this->is_range() && $this->is_list() && in_array($value, $this->value)) {
            /**
             * Добавлено неуникальное значение - оставляем метку удалить это значение при сохранении
             */
            $this->value[] = null;
            $this->modified = true;
        } else {
            $this->value[] = $value;
        }
        $this->modified = ($this->modified or $modify);
        return $this;
    }

    public function prepare_value($value)
    {
        if (is_array($value)) {
            foreach (array_keys($value) as $num) {
                $value[$num] = $this->prepare_value($value[$num]);
            }
        }
        return $value;
    }

    /**
     *
     * @param array $options аттрибуты для html тега
     * @param bool $required требовать обязательного заполнения (иначе по дефолту для аттрибута)
     * @param bool $hide прятать форму ввода (используется для аякса и только если нет установленных значений и не обязательное поле)
     *
     * @return string HTML форма ввода
     */
    public function get_form_field($options = false, $required = false, $hide = false)
    {

        $html = '';

        $required = $required || !empty($this->aScheme['required']);


        if (!$this->has_value() && $hide && !$required) {

            $html .= '<a href="javascript:void()" class="sdb_add_attr_link" data-sdb-attr="' . $this->get_name() . '" data-sdb-attr-options="' . htmlspecialchars(json_encode($options)) . '">' . __text('sdb_show_input_field') . '</a>';
            return $html;
        }
        $zero = is_array($options) && array_key_exists('zero', $options) ? $options['zero'] : false;


        $extra = $this->_get_extra($options, $required);

        if ($this->aScheme['type'] === 'text') {


            $html = form_textarea($this->get_name(), $this->get_value(), $extra);


        } elseif (in_array($this->aScheme['type'], array('decimal', 'int', 'varchar'), true) && !$this->is_multiple() && !$this->is_list() && !$this->is_source()) {


            $html = form_input($this->get_name(), $this->get_value(), $extra);
            //if (!empty($this->aScheme['suffix'])) $html.=html::span($this->aScheme['suffix'],'class="sdb_attr_suffix"');

        } elseif (in_array($this->aScheme['type'], array('decimal', 'int', 'varchar', 'bool'), true) && ($this->is_list() || $this->is_source())) {
            $_values = $this->get_stock_values();

            /*if (!empty($this->aScheme['suffix'])) {
                array_walk($_values,array($this,'_add_suffix'));
            }*/

            if ($this->is_multiple()) {
                if ($required) {
                    $extra .= ' multiple="multiple"';
                    $html = form_dropdown($this->get_name() . '[]', $_values, $this->get_value(), $extra);
                } else {

                    $html = '<div class="checkbox">' . implode('</div><div class="checkbox">', form_checkbox_group($this->get_name(), $_values, $this->get_value())) . '</div>';
                }
            } else {


                if (!isset($_values['']) && is_array($options) && $zero !== false) {
                    $_values = array('' => $zero) + $_values;
                }
                $html = form_dropdown($this->get_name(), $_values, $this->get_value(0), $extra);
            }

        } elseif ($this->is_composite()) {
            $html .= '<table class="SDB_composite_table">';

            foreach ($this->get_components() as $ext_attr_name => $ext_attr_obj) {
                $html .= '<tr>';
                $html .= '<td><label>' . $ext_attr_obj->get_title() . '</label></td>';
                $html .= '<td class="nowrap">' . $ext_attr_obj->get_form_field($options, $required) . '</td>';
                $html .= '</tr>';
            }
            $html .= '</table>';
        } elseif ($this->aScheme['type'] === 'bool') {
            $html = form_hidden($this->get_name(), '') . form_checkbox($this->get_name(), 1, $this->get_value() == 1, $extra);

        } else {
            $html = '&nbsp;';
        }


        return $html;
    }

    public function get_form_field_remove()
    {
        if ($this->is_required()) {
            return '';
        }
        return '<a href="javascript:void" class="sdb_attr_remove" data-sdb-attr="' . $this->get_name() . '" title="">' . __text('sdb_attr_remove') . '</a>';
    }

    public function _add_suffix(&$value)
    {
        $value .= ' ' . $this->aScheme['suffix'];
    }

    protected function _get_extra($options = false, $required = false)
    {
        $extra = '';

        if ($options) {
            if (!is_array($options)) {
                $options = array($options);
            }
            foreach ($options as $key => $value) {
                if ($key === 'title') {
                    $value = $this->form_prep($value);
                }
                if ($key === 'zero') {
                    $key = 'data-zero';
                }
                $extra .= ' ' . $key . '="' . $value . '"';
            }
        }

        if ($required) {
            $extra .= ' required="required" data-validate="{required:true}"';
        }

        if (isset($this->aScheme['minvalue'])) {
            $extra .= ' min="' . $this->aScheme['minvalue'] . '"';
        }

        if (isset($this->aScheme['maxvalue'])) {
            $extra .= ' max="' . $this->aScheme['maxvalue'] . '"';
        }

        if (isset($this->aScheme['minlength'])) {
            $extra .= ' minlength="' . $this->aScheme['minlength'] . '"';
        }

        if (isset($this->aScheme['maxlength'])) {
            $extra .= ' maxlength="' . $this->aScheme['maxlength'] . '"';
        }

        if (isset($this->aScheme['size'])) {
            $extra .= ' size="' . $this->aScheme['size'] . '"';
        }

        if (isset($this->aScheme['placeholder'])) {
            $extra .= ' placeholder="' . $this->aScheme['placeholder'] . '"';
        }

        return $extra;
    }

    public function get_form_hidden()
    {
        return form_hidden($this->get_name(), $this->get_value());
    }

    public function get_name()
    {
        return $this->name;
    }

    /**
     * Возвращает требуемый уровень округления десятичных значений
     *
     * @return bool|int
     */
    public function get_precision()
    {
        if ($this->get_type() != SDB_Entity::TYPE_DECIMAL) return false;
        return isset($this->aScheme['precision']) ? (int)$this->aScheme['precision'] : 2;
    }

    public function get_value($null = NULL)
    {
        $precision = $this->get_precision();
        if ($precision !== false) {
            $value = $this->get_raw_value($null);
            return is_null($value) ? $null : round($value, $precision);
        } else {
            return $this->get_raw_value($null);
        }
    }

    /**
     * Возвращает базовое значение аттрибута, с учетом дефолтного
     *
     * @param null $null
     *
     * @return mixed
     */
    public function get_raw_value($null = NULL)
    {
        if ($this->is_multiple() && !is_array($null)) {
            $null = array_filter(array($null));
        }
        if ($this->is_empty_value($this->value)) {
            return $this->is_modified() ? $null : (isset($this->aScheme['default']) ? $this->aScheme['default'] : $null);
        }

        return $this->value;
    }

    public function get_old_value()
    {

        if ($this->is_empty_value($this->old_value)) {
            return $this->is_modified() ? NULL : $this->value;
        }

        if ($this->is_multiple()) {
            array_walk($this->old_value, array($this, '_get_normalized_value'));
            return $this->old_value;
        }
        $this->_get_normalized_value($this->old_value);
        return $this->old_value;
    }

    //deprecated
    public function get_values()
    {
        return $this->get_stock_values();
    }

    public function get_ids_value()
    {
        if ($this->is_multiple()) {
            return array_combine($this->get_id(), $this->get_value());
        }
        return $this->get_value();
    }

    protected function _get_normalized_value(&$value)
    {
        if (in_array($this->get_type(), array('int', 'decimal'), true)) {
            if ($this->is_empty_value($value)) {
                $value = NULL;
            } else {
                $value = (float)str_replace(array(',', ' '), array('.', ''), $value);
                if ($value) {
                    if ($this->get_type() === 'decimal'){
                        $value = sprintf("%.4f", round($value, 4));
                    } else {
                        $value = (integer)$value;
                    }
                }
            }

        } elseif (in_array($this->get_type(), array('text', 'mediumtext', 'varchar'), true)) {
            $value = trim($value);
            $value = mb_strlen($value) ? $value : null;
        }
        return $value;
    }

    /*
     * return array
     */
    public function get_stock_values()
    {
        return isset($this->aScheme['values']) ? $this->aScheme['values'] : [];
    }

    public function set_stock_values($values)
    {
        $this->aScheme['values'] = (array)$values;
        return $this;
    }

    public function get_lists_value($key)
    {
        return isset($this->aScheme['values'][$key]) ? $this->aScheme['values'][$key] : null;
    }

    public function is_lists_key($key)
    {
        return isset($this->aScheme['values'][$key]);
    }

    public function make_value($separator = ', ')
    {
        if ($this->is_composite()) {
            $value = [];
            foreach ($this->get_components() as $attr) {

                $value[] = $attr->get_human_value();
            }
            $this->set_value(implode($separator, array_filter($value)), true);
        }
        return $this->get_value();
    }

    /**
     * Обновляет значение высчитываемых параметров
     * Безопасно для обычных параметров
     *
     * @param bool $modify
     *
     * @return $this
     */
    public function recalculate($modify = true)
    {
        $default = $this->value;
        $this->set_value(NULL, false)
            ->set_value($this->get_value($default), false)
            ->set_old_value($default)
            ->update_modified($modify);
        return $this;
    }

    public function set_human_value($value)
    {
        $this->human_value = $value;
        return $this;
    }

    /**
     * Сохраняет значение ЧЧА
     *
     * @param $value
     */
    public function add_human_value($value)
    {
        $this->human_value = $this->human_value . ($this->human_value ? ', ' : '') . $value;
    }

    /**
     * Возвращает признак наличия сохраненного значения человеко-читаемого аттрибута
     *
     * @return bool
     */
    public function has_human_value()
    {
        return !is_null($this->human_value);
    }

    /**
     * Возвращает человеко-читаемое значение аттрибута
     *
     * @return string
     */
    public function get_human_value()
    {


        if ($this->has_human_value()) return $this->human_value;
        $value = $this->get_raw_value();

        if ($this->is_list()) {
            if (!$this->has_value()) {
                return null;
            }

            if (!$this->is_multiple()) {
                return $this->get_lists_value($value);
            }

            $values = [];
            foreach ((array)$value as $val) {
                $values[] = $this->get_lists_value($val);
            }
            return implode(', ', $values);
            //return '<ul class="sdb_multiple"><li>'.implode('</li><li>',$values).'</li></ul>';


        } elseif ($this->is_composite()) {
            $html = '<table class="sdb_composite">';

            foreach ($this->get_components() as $ext_attr_name => $ext_attr_obj) {
                if (!$ext_attr_obj->has_value()) continue;
                $html .= '<tr><td>' . html::span($ext_attr_obj->get_title(), 'class="sdb_attr_title"') . '</td>';
                $html .= '<td>' . $ext_attr_obj->get_human_value() . '</td>';
                $html .= '</tr>';
            }
            $html .= '</table>';
            return $html;
        } elseif ($this->is_source()) {

            $source_table = $this->source;
            $key = $this->foreign_key;

            $source_name = $this->has_variable('foreign_field') ? $this->foreign_field : '?t.name';
            $source_name = str_replace('?t', 't', $source_name);

           return  DB::queryDB(
                "select " . $source_name . " from " . $source_table . " t where t." . $key . (is_array($value) ? ' in (?a)' : '=?') . " limit 1",
                [$value],
                'el'
            );

        } elseif ($this->get_type() === 'text') {
            return nl2br($value);
        } elseif ($this->get_type() === 'bool') {
            if (!$this->has_value()) return '?';
            return $value === 0 ? __text('no') : __text('yes');
        } elseif ($this->get_type() === 'decimal') {
            if (floor($value) == $value) {
                return number_format($value, 0, '.', ' ');
            } else {
                $str = explode('.', number_format($value, 3, '.', ' '));
                $str[1] = rtrim($str[1], '0');
                return rtrim(implode('.', $str), '.');

            }

        }

        return $value;
    }

    /**
     * Form Prep
     *
     * Formats text so that it can be safely placed in a form field in the event it has HTML tags.
     *
     * @access    public
     *
     * @param    string
     *
     * @return    string
     */

    public function form_prep($str = '')
    {
        if ($str === '') {
            return '';
        }

        $temp = '__TEMP_AMPERSANDS__';

        // Replace entities to temporary markers so that
        // htmlspecialchars won't mess them up
        $str = preg_replace("/&#(\d+);/", "$temp\\1;", $str);
        $str = preg_replace("/&(\w+);/", "$temp\\1;", $str);

        $str = htmlspecialchars($str);

        // In case htmlspecialchars misses these.
        $str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

        // Decode the temp markers back to entities
        $str = preg_replace("/$temp(\d+);/", "&#\\1;", $str);
        $str = preg_replace("/$temp(\w+);/", "&\\1;", $str);

        return $str;
    }

    public function get_table_name()
    {
        return $this->get_entity()->get_table_name_by_attr_type($this->get_type());
    }

    public function __clone()
    {
    }


    public function __get($name)
    {
        if ($this->has_variable($name)) {
            return $this->aScheme[$name];
        }
        throw new SDB_Exception('No such var: ' . $name);
    }

    public function has_variable($name)
    {
        return array_key_exists($name, $this->aScheme);
    }

    /**
     * @param $name
     * @param $args
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
        $parts = explode('_', $name);
        if ($parts[0] === 'get' && count($parts) == 2) return $this->$parts[1];
        throw new SDB_Exception('No such method: ' . $name);
    }


    public function on_delete_value($id)
    {

    }


}