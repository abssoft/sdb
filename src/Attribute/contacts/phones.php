<?php


class SDB_Attribute_contacts_phones extends SDB_Attribute_contacts {
	
	function set_value($value,$modify=true,$index=null){

		if (!is_array($value)){
			$value=str_replace(EOL,',',$value);
			$value=preg_split('~,~',$value,-1,PREG_SPLIT_NO_EMPTY);
		}

		$Contacts=new Contacts;
		$Contacts->set_special('phones',$value);
		$this->value=$Contacts->phones;
		return parent::set_value($Contacts->phones,$modify,$index);
	}
	
	function get_human_value($null=NULL){
		$value=$this->value;
		if (!is_array($value)){
			$value=str_replace(EOL,',',$value);
			$value=preg_split('~,~',$value,-1,PREG_SPLIT_NO_EMPTY);
		}

		$value=array_unique($value);
		$Contacts=new Contacts;
		$Contacts->set_special('phones',$value);
		return implode(EOL,$Contacts->get_special_phones());
	}
	
	
	
	
}