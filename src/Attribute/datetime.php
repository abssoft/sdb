<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 04.06.13
 * Time: 12:28
 */


class SDB_Attribute_datetime extends SDB_Attribute {

    const DATETIME_FORMAT='d.m.Y H:i';
    const DATETIME_PATTERN='/(\d{1,2}\.\d{1,2}\.\d{2,4})+(\s+\d{1,2}\:\d{1,2})?/';

    function get_form_field($options=false,$required=false,$hide=false){
        $required = $required || !empty($this->aScheme['required']);

		$extra=$this->_get_extra($options,$required);

        return form_input($this->get_name(),$this->get_human_value(),$extra);

    }

    function set_value($value,$modify=true,$index=null){

        $value=$this->_convert_to_unix($value);

        return parent::set_value($value,$modify,$index);
    }

    protected function _convert_to_unix($value){
        if (!is_numeric($value)){
            if (preg_match(self::DATETIME_PATTERN,$value,$matches)){
                $value=$matches[1].' '.(isset($matches[2]) ? $matches[2] : date('H:i'));

                $value=DateTime::createFromFormat(self::DATETIME_FORMAT, $value);
                $value=$value->getTimestamp();
            } else {
                $value=null;
            }
        }
        return $value;
    }

    function get_human_value($format=self::DATETIME_FORMAT){
        if (!$format) {
            $format = self::DATETIME_FORMAT;
        }
        $value=parent::get_value();

        if (!empty($value)){
            if (!is_numeric($value)){
                $value=$this->_convert_to_unix($value);
            }
            $value=date($format,$value);
        }
        return $value;

    }


}