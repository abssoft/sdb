<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 14.06.12
 * Time: 20:14
 */

class SDB_Attribute_estate_activity extends SDB_Attribute_estate {


     /*
     * return array
     */
	function get_stock_values(){
        if (!$this->get_value() or !$this->get_entity(1)->get_entity_id()) return parent::get_stock_values();
        $values=isset($this->aScheme['values']) ? $this->aScheme['values'] : array();

        if (in_array($this->get_value(),array('sell','rent'))){
            unset($values['buy'],$values['lease']);
        } else {
            unset($values['sell'],$values['rent']);
        }
        return $values;
	}

    function get_human_value(){
        $title=array(parent::get_human_value());
        $plus=$this->get_entity(1)->get_attr('estate_activityPlus');
        if ($plus->has_value()){
            $title[]=$plus->get_human_value();
        }
        sort($title);
        return implode(' или ',$title);

    }
}