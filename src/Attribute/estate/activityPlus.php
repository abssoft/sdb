<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 14.06.12
 * Time: 20:14
 */

class SDB_Attribute_estate_activityPlus extends SDB_Attribute_estate {


     /*
     * return array
     */
	function get_stock_values(){
        $ea=$this->get_entity(1)->get_attr('estate_activity')->get_value();
        if (!$ea or !$this->get_entity(1)->get_entity_id()) return parent::get_stock_values();
        $values=isset($this->aScheme['values']) ? $this->aScheme['values'] : array();


        if (in_array($ea,array('sell','rent'))){
            unset($values['buy'],$values['lease']);
        } else {
            unset($values['sell'],$values['rent']);
        }
        unset($values[$ea]);
        return $values;
	}

}