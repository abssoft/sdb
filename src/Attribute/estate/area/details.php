<?php


class SDB_Attribute_estate_area_details extends SDB_Attribute {
	
	protected static $details_names=array('total','living','kitchen');
	protected $details_values;
	function get_value_total(){
		$details=$this->extract_details();

		return isset($details['total']) ? $details['total'] : null;
	}
	
	function get_value_living(){
		$details=$this->extract_details();
		return isset($details['living']) ? $details['living'] : null;
	}
	function get_value_kitchen(){
		$details=$this->extract_details();
		return isset($details['kitchen']) ? $details['kitchen'] : null;
	}
	
	function extract_details(){
		if (is_null($this->details_values)) {
			$parts=explode('/',$this->value);
			if (isset($parts[2])) {
				$this->details_values=array_combine(self::$details_names,$parts);
			} 
		}
		return $this->details_values;
	}

    function implode_details(array $details,$modify=true){
        $value=array();
        foreach (self::$details_names as $name){
            $value[]=isset($details[$name]) ? $details[$name] : '';
        }
        $this->set_value($value,$modify);
    }

    function set_value($value,$modify=true,$index=null){
        if (is_array($value)) $value=implode('/',array_slice($value,0,3));
        $this->details_values=null;

        $total=$this->get_value_total();
        if ($total)
            $this->get_entity()->get_attr('estate_area')->set_value($total);

        return parent::set_value($value,$modify,$index);
    }

    function set_value_part($part_name,$value,$modify=true){
        if (!in_array($part_name,self::$details_names)) return trigger_error('no such part in area details');
        $details=$this->extract_details();
        $details[$part_name]=$value;
        $this->implode_details($details,$modify);
    }
	
	
	
	
	
}