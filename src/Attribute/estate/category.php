<?php


class SDB_Attribute_estate_category extends SDB_Attribute_refbook {


     function set_value($value,$modify=true,$index=null){
         if ($this->get_name()=='estate_category' && !is_numeric($value)){
             $value=$this->get_id_by_url($value);
         }
        return parent::set_value($value,$modify,$index);
     }

     /**
     * @param int $parent_id
     * @param deprecated bool $use_title
     * @return array|mixed
     */
    function get_stock_values($parent_id=''){


		$refbooks_url=$this->aScheme['refbooks_url'];
        $refbooks_parent_id=$this->get_id_by_url($refbooks_url,$parent_id);

		return DB::queryDB("select url,name from refbooks where parent_id=?i order by sort_order,name",array($refbooks_parent_id),'vars');

	}

    function get_human_value_adjusted(){
        if ($this->get_entity()->get_category()!='flat') return parent::get_human_value();


        if ($this->get_entity()->get_attr('estate_living_new')->has_value()){
            return $this->get_entity()->get_attr('estate_living_new')->get_human_value();
        }

        return parent::get_human_value();
    }


	
}