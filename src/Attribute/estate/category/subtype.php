<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 19.07.12
 * Time: 10:38
 */

/**
 * Class SDB_Attribute_estate_category_subtype
 * @uses NestedArray
 */


class SDB_Attribute_estate_category_subtype extends SDB_Attribute_estate_category {





    /**
     * @param int $parent_id
     * @return array|mixed
     */
    function get_stock_values($parent_id=''){
        $entity=$this->get_entity();




        if (!$parent_id){
            $parent_id=$entity->get_attr('estate_category_type')->get_value();
        }
        if (!$parent_id) return array();

        if (!is_array($parent_id)) $parent_id=array($parent_id);

        return DB::queryDB("select id,name from refbooks where parent_id in (?ai) order by sort_order,name",array($parent_id),'vars');

    }

    function get_form_field($options=false,$required=false,$hide=false){

        $html='';
        $required = $required || !empty($this->aScheme['required']);
        $zero=is_array($options) && array_key_exists('zero',$options) ? $options['zero'] : false;


        $extra=$this->_get_extra($options,$required);


        $entity=$this->get_entity();

        $parent_id=$entity->get_attr('estate_category_type')->get_value();
        $top_id=$entity->get_attr('estate_category')->get_value();

        if (!$parent_id) return '';

        if (!is_array($parent_id)) $parent_id=array($parent_id);

        $values=new NestedArray(DB::queryDB("select *,parent_id from refbooks where id in (?ai) or parent_id in (?ai) order by sort_order,name",array($parent_id,$parent_id),'assoc'));

        $name=$this->get_name();


        $map='';
        return '<select name="'.$name.'[]" id="'.$name.'" multiple="multiple" '.$extra.'>'.$values->getNestedDropdownOptions($top_id,$this->get_value(),$map,true).'</select>';


    }



}

