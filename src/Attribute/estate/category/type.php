<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 19.07.12
 * Time: 10:38
 */




class SDB_Attribute_estate_category_type extends SDB_Attribute_estate_category {



    /**
     * @param int $parent_id
     * @return array|mixed
     */
    function get_stock_values($parent_id=''){

        if (isset($this->aScheme['stock_values_source'])){
            if (!is_callable($this->aScheme['stock_values_source'])){
                throw new Exception('Not callable '.$this->aScheme['stock_values_source']);
            } else {
                if (is_array($this->aScheme['stock_values_source'])) {
                    $source = $this->aScheme['stock_values_source'][0];
                    $this->aScheme['stock_values_source'][0] = new $source;
                    return call_user_func_array($this->aScheme['stock_values_source'], array($this->get_entity(true), $parent_id));

                } else {
                    return call_user_func_array($this->aScheme['stock_values_source'],array($this->get_entity(true),$parent_id));
                }

            }
        }
         if (!$parent_id){
           $parent_id=$this->get_entity(true)->get_category();
        }
        $refbooks_parent_id=DB::queryDB("select id from refbooks where url=? limit 1",array($parent_id),'el');


        return DB::queryDB("select id,name from refbooks where parent_id=?i order by sort_order,name",array($refbooks_parent_id),'vars');



    }



}

