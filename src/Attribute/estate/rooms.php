<?php


class SDB_Attribute_estate_rooms extends SDB_Attribute_estate {
	
	function get_human_value($addpostfix=true){
		$value=$this->get_value('-');
		if ($this->get_entity()->get_attr('estate_roomsTo')->has_value() && $this->get_entity()->get_attr('estate_roomsTo')->get_value()>$this->get_value()){
			$value.='–'.$this->get_entity()->get_attr('estate_roomsTo')->get_value();
		}

		return $value.($addpostfix ? ' к.':'');
	}
	
	
	
}