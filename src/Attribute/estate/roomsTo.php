<?php


class SDB_Attribute_estate_roomsTo extends SDB_Attribute_estate {
	
	function set_value($value,$modify=true,$index=null){
		
		$v1=$this->get_entity()->get_attr('estate_rooms')->get_value();
		if ($v1>$value) {
			$value=null;
		}
		return parent::set_value($value,$modify,$index);
	}

    function get_human_value(){
        $v1=$this->get_entity()->get_attr('estate_rooms')->get_value();
        if ($this->get_value()==$v1) return $v1;
        return $v1.'—'.$this->get_value();
    }
	
	
	
}