<?php

class SDB_Attribute_geo extends SDB_Attribute {

    static $geo_fields_template=array('geo_country','geo_region','geo_city','geo_district','geo_street','geo_house','geo_flatnum');
    static $geo_template_default='%geo_city%, %geo_street%, %geo_house% - %geo_flatnum%';



	function get_short_title($template=null,$show_hidden=false){
        if (is_null($template)) $template=self::$geo_template_default;
		$Entity=&$this->get_entity();
        $estate_geo_visibility=$this->get_entity(true)->get_attr('estate_geo_visibility')->get_value();
        $is_parking=$this->get_entity(true)->get_category() == 'garage';
        $is_group=$this->get_entity()->get_attr('geo_group')->get_value();



        if ($show_hidden){
            $enabled_fields=array_slice(self::$geo_fields_template,0,$estate_geo_visibility);
            $disabled_fields=array_slice(self::$geo_fields_template,$estate_geo_visibility);
        } else {
            $enabled_fields=array_slice(self::$geo_fields_template,0,$estate_geo_visibility);
            $disabled_fields=array_slice(self::$geo_fields_template,$estate_geo_visibility);
        }








        $f=array();
        $r=array();
		foreach ($enabled_fields as $field){
            $f[]='%'.$field.'%';
            if ($is_parking && $field=='geo_flatnum'){
                $val='№'.$Entity->get_top_parent()->get_attr('geo_flatnum')->get_human_value();

            } else {

                $val=$Entity->get_attr($field)->get_human_value();

                 if ($field=='geo_house' && $val){
                     /**
                      * Если отсутствует номер дома, но есть номер строения (исключительный вариант)
                      */
                    if ($Entity->get_attr($field)->get_raw_value()){
                        $val='д.'.$val;
                    }
                }
                if ($field=='geo_house' && $Entity->get_top_parent()->get_attr('geo_house_construct')->get_value()){

                    $val.=' (строит.)';
                }

                if ($field=='geo_street' && $is_group){
                    $val.=' (г.з. '.$is_group.')';
                }
            }

            if ($field=='geo_city' && $Entity->get_attr('geo_village')->has_value()){
                $village=$Entity->get_attr('geo_village')->get_human_value();
                $area=$Entity->get_attr('geo_village')->get_village_area();
                //if ($area!='') $area.=', ';
                $val=$village.' ('.$area.')';//$Entity->get_attr('geo_region')->get_human_value().')';
            }

            $r[]=$val;
		}


        foreach ($disabled_fields as $field) {
            $f[]='%'.$field.'%';


            if ($is_parking && $field=='geo_flatnum'){
                $val='№'.$Entity->get_top_parent()->get_attr('geo_flatnum')->get_human_value();
            } else {
                $val=$Entity->get_attr($field)->get_human_value();
            }


            $r[]=$show_hidden && $val!='' ? ' *'.$val.'*' : '';
        }

		return trim(str_replace(' ,','',str_replace($f,$r,$template)),' ,-');
	}

    const VISIBILITY_NO = 0;
    const VISIBILITY_COUNTRY = 1;
    const VISIBILITY_REGION = 2;
    const VISIBILITY_CITY = 3;
    const VISIBILITY_DISTRICT = 4;
    const VISIBILITY_STREET = 5;
    const VISIBILITY_HOUSE = 6;
    const VISIBILITY_ALL = 7;

    function allow_export(){



        $allow=parent::allow_export();
        if (!$allow) return false;


        $visibility=$this->get_entity(true)->get_attr('estate_geo_visibility')->get_value();

        if (!$visibility) return false;

        switch ($this->get_name()){
            case 'geo_country':
                return $visibility > self::VISIBILITY_NO;
            case 'geo_region':
                return $visibility > self::VISIBILITY_COUNTRY;
            case 'geo_city':
                return $visibility > self::VISIBILITY_REGION;
            case 'geo_village':
                return $visibility > self::VISIBILITY_REGION;
            case 'geo_district':
                return $visibility > self::VISIBILITY_CITY;
            case 'geo_street':
                return $visibility > self::VISIBILITY_DISTRICT;
            case 'geo_group':
                return $visibility > self::VISIBILITY_DISTRICT;
            case 'geo_house':
                return $visibility > self::VISIBILITY_STREET;
            case 'geo_building':
                return $visibility > self::VISIBILITY_STREET;
            case 'geo_korpus':
                return $visibility > self::VISIBILITY_STREET;
            case 'geo_flatnum':
                return $visibility > self::VISIBILITY_HOUSE;

        }

        return $allow;
    }

}