<?php


class SDB_Attribute_geo_city extends SDB_Attribute_geo {

    function get_form_field($options=false,$required=false,$hide=false){
        $html='';

        $required = $required || !empty($this->aScheme['required']);



        $zero=is_array($options) && array_key_exists('zero',$options) ? $options['zero'] : false;


        $extra=$this->_get_extra($options,$required);

        $parent_id=$this->get_entity(true)->get_attr('geo_region')->get_value();

        if (!$parent_id && $this->get_default()){
            $parent_id= DB::queryDB('select region_id from ?t where id=?i limit 1', array('geo_city',$this->get_default()), 'el', null,DB_DATABASE_LIBRARY);
        }



        $_values=$this->get_stock_values($parent_id);





        if (!isset($_values['']) && is_array($options) && $zero!==false){
            $_values=array(''=>$zero)+$_values;
        }
        return form_dropdown($this->get_name(),$_values,$this->get_value(0),$extra);



    }
    function get_stock_values($parent_id=0){
		$values=array();
		if (!empty($parent_id)) {
            $clause=is_array($parent_id) ? ' in (?ai)':'=?i';

			foreach (DB::queryDB('select id,name from ?t where region_id '.$clause.' and short_name=? order by name', array('geo_city',$parent_id,'г'), 'assoc', null,DB_DATABASE_LIBRARY) as $record){
				$values[$record['id']]=$record['name'];
			}
		} 
		return $values;
	}
	private $city_cache;

	function get_special_value_city($id=0){
        if (!$id && !$this->has_human_value()) return null;
        if ($this->has_human_value()) return parent::get_human_value();

		$id=$this->get_value($id);
        $hg=new helpers_geo();
        return $id ? $hg->get_city_name($id) : null;

		//return DB::queryDB('select name from ?t where id=?i limit 1', array('geo_city',$id), 'el', null,DB_DATABASE_LIBRARY);
    }
	
	function get_human_value(){
        if ($this->has_human_value()) return parent::get_human_value();
        $hg=new helpers_geo();
        return $this->value ? $hg->get_city_name($this->value) : null;
		//return $this->value ? DB::queryDB('select name from ?t where id=?i', array('geo_city',$this->value), 'el', null,DB_DATABASE_LIBRARY) : $this->value;
	}

    function find_id_by_name($name,$region_id){
       return DB::queryDB('select id from ?t where region_id=?i and name=?', array('geo_city',$region_id,$name), 'el', null,DB_DATABASE_LIBRARY);
    }

}