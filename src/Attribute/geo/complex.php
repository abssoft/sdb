<?php


class SDB_Attribute_geo_complex extends SDB_Attribute_geo {

    protected function _is_comm(){
        return in_array($this->get_entity(true)->get_category(),array('comm','building'));
    }
	function get_stock_values($city_id=0){

        $helpers_geo=new helpers_geo();
        return $helpers_geo->find_complex($city_id,null,$this->_is_comm());

	}
    function find($city_id,$name){
        $helpers_geo = new helpers_geo();
        return $helpers_geo->find_complex($city_id,$name,$this->_is_comm());
    }



    function get_special_value($id=0){
        $id=$this->get_value($id);
        if (is_array($id)){
            return DB::queryDB('select id,name from geo_city_complex where id in (?ai) and is_comm=?i', array($id,$this->_is_comm()), 'vars', null,DB_DATABASE_LIBRARY);
        } elseif ($id) {
            return DB::queryDB('select name from geo_city_complex where id=?i and is_comm=?i limit 1', array($id,$this->_is_comm()), 'el', null,DB_DATABASE_LIBRARY);
        }
    }
	
	function get_human_value() {
        if ($this->has_human_value()) return parent::get_human_value();
		return $this->has_value() ? implode(', ',(array)$this->get_special_value($this->value)) : null;
	}



}