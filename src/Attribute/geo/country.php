<?php


class SDB_Attribute_geo_country extends SDB_Attribute_geo {
	function get_stock_values(){
		$values=array();
		foreach (DB::queryDB('select id,name from ?t order by id=?i desc,name asc', array('geo_country',Config::get('geo_country_id_russia')), 'assoc', null,DB_DATABASE_LIBRARY) as $record){
			$values[$record['id']]=$record['name'];
		}
		return $values;
	}
	
	function get_value_options(){
		
	}
	
	function get_human_value(){
        if ($this->has_human_value()) return parent::get_human_value();
		return $this->value ? DB::queryDB('select name from ?t where id=?i', array('geo_country',$this->value), 'el', null,DB_DATABASE_LIBRARY) : $this->value;
	}
}