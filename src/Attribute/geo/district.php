<?php


class SDB_Attribute_geo_district extends SDB_Attribute_geo
{

    static protected $_parents = [];

    function set_value($value, $modify = true, $index = null)
    {


        parent::set_value($value, $modify, $index);

        /**
         * При установке района, автоматически назначаем административный район
         */
        if ($this->get_name() == 'geo_district' && $modify === true) {
            $adm_attr = $this->get_entity()->get_attr('geo_district_adm');
            $value = $this->get_raw_value();
            $adm_value = $adm_attr->get_raw_value();

            if ($value) {

                if (!isset(self::$_parents[$value])) {
                    $helpers_geo = new helpers_geo();
                    $distric = $helpers_geo->get_city_district($value);
                    self::$_parents[$value] = isset($distric['parent_id']) ? $distric['parent_id'] : 0;
                }
                $fetched_parent = self::$_parents[$value];

                if ($fetched_parent) {
                    if ($adm_value != $fetched_parent) {
                        $adm_attr->set_value($fetched_parent, $modify);
                        $adm_attr->set_human_value($this->get_special_value($fetched_parent));
                    }
                } elseif ($adm_value) {
                    $adm_attr->set_value(null, true);
                }
            } elseif ($adm_value) {
                $adm_attr->set_value(null, true);
            }
        }
        return $this;
    }

    function get_stock_values($city_id = 0)
    {

        $helpers_geo = new helpers_geo();
        return $helpers_geo->find_districts($city_id);

    }

    function get_special_value($id = 0)
    {

        $id = $this->get_value($id);
        if (is_array($id)) {
            return DB::queryDB('select id,name from geo_city_districts where id in (?ai)', array($id), 'vars', null, DB_DATABASE_LIBRARY);
        } else {
            return DB::queryDB('select name from geo_city_districts where id=?i limit 1', array($id), 'el', null, DB_DATABASE_LIBRARY);
        }
    }

    function get_human_value()
    {
        if ($this->has_human_value()) return parent::get_human_value();
        return $this->has_value() ? implode(', ', (array)$this->get_special_value($this->value)) : null;
    }

}