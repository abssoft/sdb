<?php


class SDB_Attribute_geo_house extends SDB_Attribute_geo {

    /**
     * В случае строительного адреса номера дома может не быть, тогда учитываем наличия адреса строения
     *
     * @param bool $deep
     *
     * @return bool
     * @throws SDB_Exception
     */
    function has_value($deep=true){
        if (parent::has_value($deep)) return true;
        if ($this->get_entity()->get_attr('geo_house_construct')->has_value() && $this->get_entity()->get_attr('geo_building')->has_value()) return true;
        return false;
    }

    function get_value($null=NULL){
       $value=parent::get_value($null);
       $value=preg_replace('~(\s|\-|стр$)~isu','',$value);
       return $value;
    }

    function get_human_value(){
        if ($this->get_name()!='geo_house') return parent::get_human_value();
        $bld=$this->get_entity()->get_attr('geo_building')->get_value();
        return $this->value.($bld ? (', стр.'.$bld):'');
	}
}

