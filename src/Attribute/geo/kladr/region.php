<?php


class SDB_Attribute_geo_kladr_region extends SDB_Attribute_geo {

	function get_stock_values($parent_id=0){

		$values=array();
		if ($parent_id>0) {
			$values=DB::queryDB('select id,concat(name,\' \',short_name) as name from ?t where country_id=?i order by name', array('geo_region',$parent_id), 'vars', null,DB_DATABASE_LIBRARY);
			
		} 
		return $values;
	}
		
	
	function get_human_value(){

        if (!$this->value) return $this->value;
        $row=DB::queryDB("select name,short_name from ?t where id=?i", array('geo_region',$this->value), 'rowassoc', null,DB_DATABASE_LIBRARY);
        $name=$row['name'];
        switch ($row['short_name']){
            case 'г':
            case 'Чувашия':
                break;
            case 'край';
                $name.=' '.$row['short_name'];
            default:
                $name.=' '.$row['short_name'].'.';
        }
        return $name;
	}
}