<?php


class SDB_Attribute_geo_kladr_street extends SDB_Attribute_geo {



	function clean_street_name($name){
       $name=str_replace(array('бульвар','проспект','улица','ул.','переулок','квартал'),array('б-р','пр-кт','ул','ул','пер','кв-л'),$name);
       if (preg_match('~(\d+)-а?я [Лл]иния~isu',$name,$matches)){
          $name='Линия '.$matches[1].'-я';
       }
        if (preg_match('~(\d+)-я [Рр]абочая~isu',$name,$matches)){
            $name='Рабочая '.$matches[1].'-я';
        }
        if (preg_match('~(\d+)-й кв-л~isu',$name,$matches)){
            $name='15 Микрорайон '.$matches[1].'-й кв-л';
        }
        return $name;
    }

    function find_id_by_name($name,$city_id){
        return DB::queryDB('select id from ?t where city_id=?i and concat(short_name,\' \',name,\' \',short_name) like (?)', array('geo_street',$city_id,'%'.$name.'%'), 'el', null,DB_DATABASE_LIBRARY);
    }

    function find_street_name($id){
        return DB::queryDB("select concat(short_name, ' ',name) from ?t where id=?i", array('geo_street',$id), 'el', null,DB_DATABASE_LIBRARY);
    }
}