<?php


class SDB_Attribute_geo_metro extends SDB_Attribute_geo {


	function get_stock_values($city_id=0){

        $helpers_geo=new helpers_geo();
        return $helpers_geo->find_metro($city_id,null);

	}
    function find($city_id,$name){
        $helpers_geo = new helpers_geo();
        return $helpers_geo->find_metro($city_id,$name);
    }



    function get_special_value($id=0){
        $id=$this->get_value($id);
        if (is_array($id)){
            return DB::queryDB('select id,name from geo_city_metro where id in (?ai)', array($id), 'vars', null,DB_DATABASE_LIBRARY);
        } elseif ($id) {
            return DB::queryDB('select name from geo_city_metro where id=?i limit 1', array($id), 'el', null,DB_DATABASE_LIBRARY);
        }
    }
	
	function get_human_value() {
        if ($this->has_human_value()) return parent::get_human_value();
		return $this->has_value() ? implode(', ',(array)$this->get_special_value($this->value)) : null;
	}



}