<?php


class SDB_Attribute_geo_region extends SDB_Attribute_geo {

	function get_stock_values($parent_id=0){

		$values=array();
		if ($parent_id>0) {
			$values=DB::queryDB('select id,concat(name,\' \',short_name) as name from ?t where country_id=?i order by name', array('geo_region',$parent_id), 'vars', null,DB_DATABASE_LIBRARY);
			
		} 
		return $values;
	}
		
	function get_value($null=NULL){
        if ($this->has_value()) return parent::get_value($null);

        $city_id=$this->get_entity()->get_attr('geo_city')->get_value();
        if (!$city_id) return parent::get_value($null);
        $hg=new helpers_geo();
        return $hg->get_region_id_by_city_id($city_id);

        //return DB::queryDB("select region_id from ?t where id=?i", array('geo_city',$city_id), 'el', null,DB_DATABASE_LIBRARY);
    }

    function get_human_value(){

        if ($this->has_human_value()) return parent::get_human_value();

        if (!$this->value) return null;
        $row=DB::queryDB("select name,short_name from ?t where id=?i", array('geo_region',$this->value), 'rowassoc', null,DB_DATABASE_LIBRARY);
        $name=$row['name'];
        switch ($row['short_name']){
            case 'г':
            case 'Чувашия':
                break;
            case 'край';
                $name.=' '.$row['short_name'];
                break;
            case 'обл';
            case 'область';
                $name.=' область';
                break;
            default:
                if ($row['short_name']!='') {
                    $name .= ' ' . $row['short_name'] . '.';
                }
        }
        return $name;
    }
}