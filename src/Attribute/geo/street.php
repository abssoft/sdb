<?php


class SDB_Attribute_geo_street extends SDB_Attribute_geo {


    function get_form_field($options=false,$required=false,$hide=false){
        $html='';

        $required = $required || !empty($this->aScheme['required']);

        $extra=$this->_get_extra($options,$required);


        return form_input($this->get_name(),$this->get_human_value(),$extra);
    }

	function clean_street_name($name){
        $name=str_replace(array(','),'',$name);
        $name=strtr($name,array_combine(
            array('бульвар','Пр-кт','проспект','улица','Ул.','ул.','Ул ','переулок','квартал'),
            array('б-р','пр-кт','пр-кт','ул','ул ','ул ','ул ','пер','кв-л')));
        $name=preg_replace('~\s{2,}~s',' ',trim($name));
      /* if (preg_match('~(\d+)-а?я [Лл]иния~isu',$name,$matches)){
          $name='Линия '.$matches[1].'-я';
       }
        if (preg_match('~(\d+)-я [Рр]абочая~isu',$name,$matches)){
            $name='Рабочая '.$matches[1].'-я';
        }
        if (preg_match('~(\d+)-й кв-л~isu',$name,$matches)){
            $name='15 Микрорайон '.$matches[1].'-й кв-л';
        }*/
        return $name;
    }


    function find_id_by_name($name,$city_id){

        $name=$this->clean_street_name($name);
        $cities=DB::queryDB('select id,name,short_name from ?t where city_id=?i and concat(short_name,\' \',name,\' \',short_name) like (?)', array('geo_street',$city_id,'%'.$name.'%'), 'assoc', null,DB_DATABASE_LIBRARY);

        foreach ($cities as $record){
            if (mb_strtolower($record['name'])==mb_strtolower($name) || mb_strtolower($record['short_name'].' '.$record['name'])==mb_strtolower($name)){
                return $record['id'];
            }
        }


    }

    function find_name_by_id($id){
        return DB::queryDB("select concat(short_name,' ',name) from ?t where id=?i", array('geo_street',$id), 'el', null,DB_DATABASE_LIBRARY);
    }

    function find_street_name($id){
        return DB::queryDB("select concat(short_name, ' ',name) from ?t where id=?i", array('geo_street',$id), 'el', null,DB_DATABASE_LIBRARY);
    }

    function get_value($null=NULL){

        $value=parent::get_value($null);

        if ($this->get_entity()->is_empty_value($value)) return $null;

        if (is_numeric($value)) return $value;


        $city_id=$this->get_entity()->get_attr('geo_village')->get_value();
        if (!$city_id) $city_id=$this->get_entity()->get_attr('geo_city')->get_value();
        if (!$city_id) return $null;

        return $this->find_id_by_name($value,$city_id);
    }

    function get_human_value(){


        if ($this->has_human_value()) return parent::get_human_value();

        $value=parent::get_value();

        if ($this->get_entity()->is_empty_value($value)) return null;

        if (!is_numeric($value)) return $value;
        return $this->find_name_by_id($value);
    }
}