<?php


class SDB_Attribute_geo_village extends SDB_Attribute_geo_city {


    function get_stock_values($parent_id=0){
		$values=array();
		if (!empty($parent_id)) {
            $clause=is_array($parent_id) ? ' in (?ai)':'=?i';

			foreach (DB::queryDB('select id,name from ?t where region_id '.$clause.' and short_name<>? order by name', array('geo_city',$parent_id,'г'), 'assoc', null,DB_DATABASE_LIBRARY) as $record){
				$values[$record['id']]=$record['name'];
			}
		} 
		return $values;
	}

	
	function get_human_value(){
        if ($this->has_human_value()) return parent::get_human_value();
        if (!$this->value) return null;
        $hg=new helpers_geo();
        $city=$hg->get_city($this->value);

        if (isset($city['short_name'])){
            return $city['short_name'].'. '.$city['name'];
        }
        return $city['short_full_name'].' '.$city['name'];
		//return $this->value ? DB::queryDB('select name from ?t where id=?i', array('geo_city',$this->value), 'el', null,DB_DATABASE_LIBRARY) : $this->value;
	}

    function get_village_area(){
        $hg=new helpers_geo();
        $city=$hg->get_city($this->value,1);
        return $city['area_name'];
    }

    function find_id_by_name($name,$region_id){
       return DB::queryDB('select id from ?t where region_id=?i and name=?', array('geo_city',$region_id,$name), 'el', null,DB_DATABASE_LIBRARY);
    }

}