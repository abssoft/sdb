<?php
/**
 * Позволяет хранить описание объектов и массивов
 * Class SDB_Attribute_json
 */
class SDB_Attribute_json extends SDB_Attribute
{
    function set_value($value, $modify = true, $index = null)
    {
        $this->value = json_encode($value);
        $this->update_modified($modify);
        return $this;
    }

    function get_value($null = null)
    {
        return json_decode(parent::get_value($null), true);
    }
}