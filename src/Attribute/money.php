<?php

class SDB_Attribute_money extends SDB_Attribute
{

    function get_human_value($use_currency = true)
    {

        $value = $this->get_value();
        $result = self::number_format($value);
        if ($use_currency) {
            $group_name = strstr($this->get_name(), '_', true);
            if ($this->get_entity(true)->attr_exists($group_name . '_currency')) {
                $currency = $this->get_entity(true)->get_attr($group_name . '_currency');
                return $result . ' ' . $currency->get_human_value() . '';

            } elseif ($this->get_suffix()) {
                return $result . ' ' . $this->get_suffix() . '';
            }
        }
        return $result;

    }

    static function number_format($value)
    {
        return number_format((float)$value, floor($value) != $value ? 2 : 0, '.', ' ');
    }
}
