<?php

/**
 * Позволяет хранить длинный текст чанками в любом порядке
 * и при этом собирать текст из чанков в нужном порядке
 * Class SDB_Attribute_multitext
 */
class SDB_Attribute_multitext extends SDB_Attribute
{

    function is_multiple()
    {
        return true;
    }

    function set_value($value, $modify = true, $index = null)
    {
        $this->set_old_value($this->value);
        $this->value = $this->pack($value);
        $this->update_modified($modify);
        return $this;
    }

    function get_value($null = null)
    {
        return $this->unpack(parent::get_value($null));
    }

    function get_human_value()
    {
        return implode('',(array)$this->get_value());
    }

    /**
     * @param $value
     * @return array
     */
    protected function pack($value){
        if (is_array($value)) {
            $value = implode('', array_values($value));
        }
        $resvalue = [];
        foreach ($this->mb_str_split(mb_convert_encoding($value,'UTF-8'), 200) as $num => $item) {
            $resvalue[] = serialize([$num, $item]);
        }
        return $resvalue;
    }
    protected function unpack($value){
        $value = (array)$value;
        $textChunks = [];
        foreach ($value as $item){
            if (strpos($item,'a:')===0) {
                $item = @unserialize($item);
                if (isset($item[1])) {
                    $textChunks[$item[0]] = $item[1];
                }
            } else {
                $textChunks[] = $item;
            }
        }
        return $textChunks;
    }

    protected function mb_str_split($string, $split_length = 200)
    {
        if ($split_length === 1) {
            return preg_split("//u", $string, -1, PREG_SPLIT_NO_EMPTY);
        } elseif ($split_length > 1) {
            $return_value = [];
            $string_length = mb_strlen($string, "UTF-8");
            for ($i = 0; $i < $string_length; $i += $split_length) {
                $return_value[] = mb_substr($string, $i, $split_length, "UTF-8");
            }
            return $return_value;
        } else {
            return false;
        }
    }

}