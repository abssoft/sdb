<?php


class SDB_Attribute_phones extends SDB_Attribute
{


    /**
     * @param      $value
     * @param bool $modify
     * @param null $index
     *
     * @return SDB_Attribute
     */
    function set_value($value, $modify = true, $index = null)
    {
        if (!is_array($value)) {
            $value = preg_split('~[,\n\r]~', $value, -1, PREG_SPLIT_NO_EMPTY);
        }

        $value = array_unique(array_filter(array_map('plain_phone', $value)));

        if (!$this->is_multiple()) {
            $value = implode(',', $value);
        }

        return parent::set_value($value, $modify, $index);
    }


    function get_human_value()
    {
        if ($this->is_multiple()) {
            return implode(PHP_EOL, array_map('format_phone', array_unique((array)$this->get_value())));

        } else {
            return format_phone($this->value);
        }
    }


}