<?php


/**
 * Description of progress
 *
 * @author Артем
 */
class SDB_Attribute_projects_files extends SDB_Attribute_projects {
	
	function get_stock_values($projects_id=0){
		$ProjectsFiles=new ProjectsFiles;
		return $ProjectsFiles->get_projects_files($projects_id);
	}
	
	function get_special_value_city($id){
		if (!is_array($id)) $id=array($id);
		return DB::queryDB('select * from ?t where id in (?ai)', array($id), 'assoc');
	}
	
	function get_human_value(){
		return $this->value ? DB::queryDB('select name from city where id=?i', array($this->value), 'el', null,DB_DATABASE_LIBRARY) : $this->value;
	}
	
		
	function get_form_field($options = false, $required = false, $hide = true) {
		$projects_id=$this->get_entity()->get_top_parent()->get_entity_id();
		$html='';
		
		$html.='<button type="button" data-projects_id="'.$projects_id.'" data-field_name="'.$this->get_name().($this->is_multiple() ? '[]':'').'" onClick="ProjectsFiles.choose_files(event,this);" class="button btn btn-default btn-sm">Прикрепить файл'.($this->is_multiple() ? 'ы':'').'</button>';
	
		$extra='';
		if ($options){
			if (!is_array($options)) $options=array($options);
			foreach ($options as $key=>$value){
				if ($key=='title') $value=form_prep($value);
				$extra.=' '.$key.'="'.$value.'"';
			}
    	}
		 if ($required){
			  $extra.=' required="true" data-validate="{required:true}"';
		 }
		 $html.=$this->is_multiple() ? '<ol class="projects_files_list">' : '<ul class="projects_files_list">';
		 	
		 if ($this->has_value()){
		   $values=$this->get_value();

		   
		   $files_records=$this->get_stock_values($projects_id);
		   
		   if (!is_array($values)) $values=array($values);
	
		   foreach ($values as $num=>$value){
			   $multiples=($this->is_multiple() ? '['.$num.']':'');
			   if (isset($files_records[$value])){
				   $file=$files_records[$value];

				   $view_url=Controllers_account_projects::get_files_show_url($projects_id,$file['id'],$file['file_url']);
				   $html.='<li>'.html::a($view_url,ProjectsFiles::filename_wordwrap($file['file_name'])).' | <input type="checkbox" name="'.$this->get_name(). $multiples.'" value="">Убрать?';
			   
			   } else {
				     $html.='<li>Файл удален <input type="hidden" name="'.$this->get_name().$multiples.'" value="" ></li>';
			   }
			   $html.='</li>';
			 
			   /*
			 $viewlink='<a href="/account/projects/edit/'.$projects_id.'/get_file.html?name='.$value.'">Загрузить</a>';
			 $viewlink='<a target=_blank href="/upload/projects/'.$this->get_entity()->get_top_parent()->get_entity_id().'/files/'.$value.'">Посмотреть</a>';
			 if ($this->is_multiple()) {
			   $html.='<li>Файл '.($num+1).' | '.$viewlink.' '.'| <input type="checkbox" name="'.$this->get_name().'['.$num.']" value="">Удалить?';
			 } else {
			   $html.='<li>Файл | '.$viewlink.' '.'| <input type="checkbox" name="'.$this->get_name().'" value="">Удалить?';
			 }*/
		   }
	
		 }
	
	
		/* if ($this->is_multiple()) {
			 
	
			 $html.='<li class="SDB_files_input"><input id="'.$this->get_name().'" type="file" name="'.$this->get_name().'[]" multiple="multiple"></li>';
		 } else {
			 $html.='<li class="SDB_files_input"><input id="'.$this->get_name().'" type="file" name="'.$this->get_name().'"></li>';
		 }*/
		  $html.=$this->is_multiple() ? '</ol>' : '</ul>';
	
		/* if ($this->is_multiple()){
			$html.='<a href="javascript:void()" data-domcopy=".SDB_files_input" class="SDB_add_field_button">Добавить файл</a><br>';
		 }*/
		 
		 
	
	
		 return $html;

  }
}