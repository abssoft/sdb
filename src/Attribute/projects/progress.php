<?php


/**
 * Description of progress
 *
 * @author Артем
 */
class SDB_Attribute_projects_progress extends SDB_Attribute_projects {
	
	function get_percent_value(){
		$cmps=$this->get_components();
		$ready=0;
		foreach ($cmps as $c){
			if ($c->has_value(true)) $ready++;
		}
		
		return 100*round($ready/sizeof($cmps),2);

		
	}
	
	function get_form_field($options=false,$required=false,$hide=true){
		if ($this->get_type()!='function') return parent::get_form_field($options,$required,$hide);
		
		$fields=array('<ul>');
		foreach($this->get_stock_values() as $attr_name=>$unused_yet){
		  $attr=$this->get_entity()->get_top_parent()->get_attr($attr_name);
		  if ($attr->has_value()){
			$fields[]='<li class="SDB_list_ok"><span class="SDB_list_result">&radic;</span> <s>'.$attr->get_title().'</s></li>';
		  } else {
			$fields[]='<li class="SDB_list_error"><span class="SDB_list_result">&times;</span> '.$attr->get_title().'</li>';
		  }
		}
		$fields[]='</ul>';
		return implode('',$fields);
	  }
	
	 
	  function has_value($deep=true){
		foreach($this->get_stock_values() as $attr_name=>$unused_yet){
		  if (!$this->get_entity()->get_top_parent()->get_attr($attr_name)->has_value()) return false;
		}
		return true;
	
	  }
  
  
//
//  function prepare_value($value){
//      if ($this->get_type()!='file') return parent::prepare_value($value);
//
//	   if (is_array($value)){
//			foreach (array_keys($value) as $num){
//				$value[$num]=$this->prepare_value($value[$num]);
//			}
//		}
//		return $value==''?null:$value;
//	}

  /*function get_form_field($options = false, $required = false, $hide = true) {
    if ($this->get_type()!='file') return parent::get_form_field($options, $required, $hide);

    $html='';

    $extra='';
    if ($options){
        if (!is_array($options)) $options=array($options);
        foreach ($options as $key=>$value){
            if ($key=='title') $value=form_prep($value);
            $extra.=' '.$key.'="'.$value.'"';
        }
    }
     if ($required){
          $extra.=' required="true" data-validate="{required:true}"';
     }
     $html.='<ol>';

     if ($this->has_value()){
       $values=$this->get_value();
       if (!is_array($values)) $values=array($values);

       foreach ($values as $num=>$value){
         $viewlink='<a href="/account/projects/edit/'.$this->get_entity()->get_top_parent()->get_entity_id().'/get_file.html?name='.$value.'">Загрузить</a>';
         $viewlink='<a target=_blank href="/upload/projects/'.$this->get_entity()->get_top_parent()->get_entity_id().'/files/'.$value.'">Посмотреть</a>';
         if ($this->is_multiple()) {
           $html.='<li>Файл '.($num+1).' | '.$viewlink.' '.'| <input type="checkbox" name="'.$this->get_name().'['.$num.']" value="">Удалить?';
         } else {
           $html.='<li>Файл | '.$viewlink.' '.'| <input type="checkbox" name="'.$this->get_name().'" value="">Удалить?';
         }
       }

     }


     if ($this->is_multiple()) {

      $html.='<li class="SDB_files_input"><input id="'.$this->get_name().'" type="file" name="'.$this->get_name().'[]" multiple></li>';
     } else {
      $html.='<li class="SDB_files_input"><input id="'.$this->get_name().'" type="file" name="'.$this->get_name().'"></li>';
     }
     $html.='</ol>';

     if ($this->is_multiple()){
        $html.='<a href="javascript:void()" data-domcopy=".SDB_files_input" class="SDB_add_field_button">Добавить файл</a><br>';
     }


     return $html;

  }*/


}

?>
