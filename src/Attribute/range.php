<?php


class SDB_Attribute_range extends SDB_Attribute {
	
	
	function is_multiple(){
		return true;
	}
	
	function has_value($deep=true){
		if (!is_array($this->value)) return false;
		$v=array_values($this->value);
		
		return !empty($v[0]) or !empty($v[1]);
	}
    function set_value($value,$modify=true,$index=null){
        /*
         * Во избежание потери младшей границы диаппазона
         */
        if (is_array($value)){
            $value=array_map('trim',array_values($value));
            if (isset($value[1]) && (!isset($value[0]) or $value[0]==='')){
                $value[0]=0;
            }
        }
        return parent::set_value($value,$modify,$index);
    }
	
	function get_value($type=null){


        if (is_null($this->value) or !is_array($this->value)) return NULL;
        $value = array_map('trim', array_values($this->value));
        $precision = $this->get_precision();
        if ($precision !== false) {
            $value = array_map(function ($val) use ($precision) {
                return round($val, $precision);
            }, $value);
        }

		if (is_null($type)){
			



            if (count($value)>1){
                $value=array_combine(array('min','max'),array_slice($value,0,2));
                if (!array_sum($value)){
                    $value=array();
                }
            } elseif (!empty($value[1])){
                $value=array('min'=>0,'max'=>$value[1]);
            } elseif (!empty($value[0])) {
                $value=array('min'=>$value[0]);
            } else {
                $value=array();
            }

			return $value;
		} elseif (is_numeric($type)){
			$type2=$type==0 ? 'min':'max';
		} else {
			$type2=$type=='min' ? 0:1;
		}
		if (!empty($value[$type])){
			return $value[$type];
		} elseif (!empty($value[$type2])){
			return $value[$type2];
		} else {
		    return null;
        }
	}
	function get_human_value($divider='–'){
		if (!$this->has_value()) return '';

		if ($this->get_value('min')==$this->get_value('max')){
			return $this->get_value('min');
		} elseif ($this->get_value('max')){
            $min = $this->get_value('min');
            $min = $min ? $min.$divider : 'до ';
			return $min .$this->get_value('max');
		} elseif ($this->get_value('min')) {
			return $this->get_value('min').'+';
		} 
	}
	
	function get_form_field($options=false,$required=false,$hide=false){

        $html='';

        $required = $required || !empty($this->aScheme['required']);

		$extra=$this->_get_extra($options,$required);
		if (!is_array($this->value)) $this->value=array($this->value);

		array_walk($this->value,array($this,'_get_normalized_value'));

		
		$html=form_input($this->get_name().'[min]',$this->get_value('min'),str_replace('%suffix%','min',$extra).' placeholder="от"').'—'.form_input($this->get_name().'[max]',$this->get_value('max'),str_replace('%suffix%','max',$extra).' placeholder="до"');
		return $html;
	}

    /**
     * @param $value
     * @param int $accuracy точность в %
     * @return bool
     */
    function in_range($value,$accuracy=0){

        $min=$this->get_value('min');
        $max=$this->get_value('max');
        if ($accuracy){
            $accuracy=$accuracy/100;

            $min=is_null($min) ? $value : $min*(1-$accuracy);
            $max=is_null($max) ? $value : $max*(1+$accuracy);
        }




        if (is_array($value)){
            $value=array_values($value);

            if (isset($value[1])){
                return ($min<=$value[0] && $max>=$value[0]) or ($min<=$value[1] && $max>=$value[1]);
            } else {
                return true;
            }
        } else {
            return ($min<=$value && $max>=$value);
        }
    }
}