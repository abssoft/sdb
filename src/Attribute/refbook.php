<?php

class SDB_Attribute_refbook extends SDB_Attribute
{

    static $_cache;
    static $_url_idx;

    function __construct($attr_name, $aScheme)
    {
        parent::__construct($attr_name, $aScheme);
        if (is_array($this->aScheme) && !array_key_exists('refbooks_url', $this->aScheme)) {
            throw new Exception(__text('No refbooks_url given in attr scheme!'));
        }
    }

    function is_list()
    {
        return true;
    }

    function is_refbook()
    {
        return true;
    }

    //см update_all_attributes_values
    //NOTE: если значение берется из Refbooks то is_lists_key = true
    //TODO: подумать над кэшированием и нарузкой если проверять

    function is_lists_key($key)
    {
        return $key > 0;
    }

    function get_lists_value($key)
    {

        $refbooks_url = $this->aScheme['refbooks_url'];
        if (is_array($key)) {
            return DB::queryDB("select name from refbooks where id in (?ai) order by FIELD (id,?ai)", array($key, $key), 'col');
        } else {
            return DB::queryDB("select name from refbooks where id=?i", array($key), 'el');
        }


    }

    function get_human_value()
    {


        if (!$this->has_human_value() && $this->has_value()) {


            $this->human_value = $this->get_lists_value($this->get_value());

        }

        if (is_array($this->human_value)) {
            $this->human_value = implode(', ', $this->human_value);
        }
        return $this->human_value;
    }


    function get_id_by_url($url, $parent_id = null)
    {
        return $this->get_field_by_url('id', $url, $parent_id);
    }

    function get_field_by_url($field, $url, $parent_id = null)
    {
        if (empty($url)) return null;
        $parent_id = (integer)$parent_id;
        if (!isset(self::$_cache[$parent_id][$url])) {
            $this->_load_into_cache($parent_id, $url);
        }
        return $this->_get_from_cache($parent_id, $url, $field);
    }

    function _load_into_cache($parent_id, $url)
    {
        if ($parent_id) {
            self::$_cache[$parent_id][$url] = DB::queryDB("select * from refbooks where parent_id=?i and url=? limit 1", array($parent_id, $url), 'rowassoc');
        } else {

            self::$_cache[$parent_id][$url] = DB::queryDB("select * from refbooks where url=? limit 1", array($url), 'rowassoc');
        }
    }

    function _get_from_cache($parent_id, $url, $field)
    {
        return isset(self::$_cache[$parent_id][$url][$field]) ? self::$_cache[$parent_id][$url][$field] : null;
    }

    function get_name_by_url($url)
    {
        return $this->get_field_by_url('name', $url);
    }

    function get_url_by_id($id)
    {
        if (!isset(self::$_url_idx[$id])) {
            self::$_url_idx[$id] = DB::queryDB("select url from refbooks where id=?i", array($id), 'el');
        }
        return self::$_url_idx[$id];
    }


    /**
     * @param int $parent_id
     * @param     deprecated bool $use_title
     *
     * @return array|mixed
     */
    function get_stock_values($parent_id = '')
    {


        $refbooks_url = $this->aScheme['refbooks_url'];
        $refbooks_parent_id = $this->get_id_by_url($refbooks_url, $parent_id);

        return DB::queryDB("select id,name from refbooks where parent_id=?i order by sort_order,name", array($refbooks_parent_id), 'vars');

    }

    function set_value($value, $modify = true, $index = null)
    {
        $this->set_human_value(NULL);
        return parent::set_value($value, $modify, $index);

    }


}

