<?php

class SDB_Attribute_user extends SDB_Attribute {
	
	function get_fio(){
		$Entity=&$this->get_entity();
		return implode(' ',array_filter(array($Entity->get_attr('user_name_last')->get_human_value(),$Entity->get_attr('user_name_first')->get_human_value(),$Entity->get_attr('user_name_middle')->get_human_value())));
		
	}
}