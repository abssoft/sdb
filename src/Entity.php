<?php

defined('SDB_DIR') || define('SDB_DIR', realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
defined('SDB_PHP_EXT') || define('SDB_PHP_EXT', '.php');

/**
 * 02.12.2012
 * @require DB (goDB)
 * todo: remove external requirements
 */
class SDB_Entity
{

    /**
     * Имя сущности
     *
     * @var string
     *
     */
    protected $namespace;


    /**
     * Стек аттрибутов сущности, каждый аттрибут представляет собой
     * объект соответствующего класса
     */

    /**
     * @var SDB_Attribute[]
     */
    protected $attributes = [];

    /**
     * Ссылки на внешние подключенные сущности
     *
     * @var SDB_Entity[]
     */

    protected $external = [];

    protected static $SchemesDir;

    /**
     * Директория подключения локальных классов аттрибутов
     *
     * @var
     */
    protected static $autoloadDir;


    protected $values = [];
    protected $values_modified = [];
    protected $defaults = [];

    private $filter_sep = '=';
    protected $attribute_filter;
    protected $basetable;
    protected $entity_id;

    protected $company_id;

    protected $category;

    protected $debug_func;

    /**
     * @var SDB_Entity
     */
    private $parent_entity;

    /**
     *  Соответствие названия таблиц типам данных
     *  в обычной конфигурации выделяется три таблицы типов
     *  с максимально несовместимыми типами данных
     *  во избежание ситуации смены типа аттрибута
     */
    protected static $table_map = array(
        'int' => 'int',
        'bool' => 'int',
        'text' => 'varchar',
        'decimal' => 'decimal',
        'varchar' => 'varchar'
    );
    /**
     * @var array $attr_names ;
     * map of attr_id=>attr_name
     */
    private $attr_names;

    const TYPE_INT = 'int';
    const TYPE_BOOL = 'bool';
    const TYPE_TEXT = 'text';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_VARCHAR = 'varchar';


    /**
     *
     * @param string $name Имя сущности
     *
     * Массив подключаемых аттрибутов
     * null - подключаются все
     * @param string $filter
     */
    function __construct($name = null, $filter = null)
    {
        if (is_array($filter))
            $this->set_attribute_filter($filter);

        if (!is_null($name)) {
            $this->add_schema($name);
            if (strpos($name, ':') === false and $this->basetable == '') {
                $this->set_base_table($name);
            }
        }
    }

    static function set_scheme_dir($dir)
    {
        self::$SchemesDir = $dir;
    }

    static function set_autoload_dir($dir)
    {
        self::$autoloadDir = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    static function get_autoload_dir()
    {
        return self::$autoloadDir;
    }


    /**
     * Подключание схемы аттрибутов
     *
     * @param string $name Имя схемы
     *
     * @return SDB_Entity
     */
    function add_schema($name, $schema = null)
    {

        if (strpos($name, ':') !== false) {
            $names = [];
            foreach (explode(':', $name) as $nspart) {
                $names[] = $nspart;
                $this->add_schema(implode('_', $names), $schema);
            }
            return $this;
        }
        $this->namespace = $name;


        if (is_null($schema)) {
            $schema = $this->get_schema($name);
        }


        foreach (array_keys($schema) as $attr_name) {


            $this->attributes[$attr_name] = SDB_Attribute::factory($attr_name, $this->parse_scheme_attribute($schema[$attr_name], $attr_name), $this);


            if ($this->attributes[$attr_name]->get_type() == 'external') {

                foreach (array_keys($this->external[$attr_name]->get_attributes()) as $components_name) {
                    $this->attributes[$components_name] =& $this->external[$attr_name]->get_attr($components_name);
                }


            } elseif ($this->attributes[$attr_name]->get_type() == 'composite') {

                $this->external[$attr_name] = new self();
                $this->external[$attr_name]->set_parent($this);


                $this->external[$attr_name]->add_schema($attr_name, $this->attributes[$attr_name]->components);

                foreach (array_keys($this->external[$attr_name]->get_attributes()) as $components_name) {
                    $this->attributes[$components_name] =& $this->external[$attr_name]->get_attr($components_name);

                }
                foreach (array_keys($this->get_attr($attr_name)->components) as $components_name) {
                    $this->attributes[$attr_name]->add_component($components_name, $this->attributes[$components_name]);
                }

            } elseif (is_array($this->attribute_filter) && !in_array($attr_name, $this->attribute_filter)) {
                unset($this->attributes[$attr_name]);
                continue;
            }
        }
        foreach (array_keys($schema) as $attr_name) {

            if (is_object($this->attributes[$attr_name])) {
                $this->attributes[$attr_name]->set_default();
            }
        }


        return $this;
    }

    private function &parse_scheme_attribute(&$attribute, $attr_name, $component = false)
    {
        $namespace = $this->get_namespace($attr_name);
        if ($attribute == 'external') {
            if (!isset($this->external[$namespace])) {

                $this->external[$namespace] = new self($namespace);


                $this->external[$namespace]->set_parent($this);

                $this->external[$namespace]->set_base_table($this->namespace);

            }
            // при подключении внешнего файла, не содержащего собственной схемы
            if ($attr_name != $namespace) {
                $attribute = &$this->external[$namespace]->get_scheme_attribute($attr_name, $namespace);
            } else {
                $attribute = array('type' => 'external');
            }
        }


        return $attribute;
    }

    function set_parent(&$entity)
    {
        $this->parent_entity =& $entity;
    }

    function &get_top_parent()
    {
        $parent = is_null($this->parent_entity) ? $this : $this->parent_entity->get_top_parent();
        return $parent;
    }

    /**
     * @param $namespace
     *
     * @return SDB_Entity;
     */
    function get_external($namespace)
    {
        return $this->external[$namespace];
    }

    function &get_scheme_attribute($attr_name, $namespace = false)
    {

        if (!$namespace) {
            $namespace = $this->get_namespace($attr_name);
        }
        $attr = null;

        if (isset($this->external[$namespace])) {
            $attr = &$this->external[$namespace]->get_scheme_attribute($attr_name, $namespace);
        } elseif ($namespace == $this->namespace) {
            $attr = &$this->attributes[$attr_name];

        }
        return $attr;
    }

    function get_namespace($attr_name)
    {
        if (strpos($attr_name, '_') === false) return $attr_name;
        return implode('_', array_slice(explode('_', $attr_name), 0, -1));
    }

    function set_attribute_filter($filter)
    {
        $this->attribute_filter = $filter;
    }

    /**
     * @param $name
     * @return mixed
     * @throws SDB_Exception
     */
    static function get_schema($name)
    {
        if (!is_file(self::_get_schema_filename($name))) {
            throw new SDB_Exception('There is no schema ' . $name);
        }
        return include(self::_get_schema_filename($name));
    }

    protected static function _get_schema_filename($url)
    {
        return self::$SchemesDir . 'DataSchemes/' . str_replace('_', DIRECTORY_SEPARATOR, $url) . '.php';
    }

    /**
     * @param $attr_name
     * @return bool
     */
    function attr_is_multiple($attr_name)
    {
        return $this->attributes[$attr_name]->is_multiple();
    }

    /**
     * @param $attr_name
     * @return bool
     */
    function attr_is_list($attr_name)
    {
        return $this->attributes[$attr_name]->is_list();
    }

    /**
     * @param $attr_name
     * @return float|mixed
     */
    function attr_get_stock_values($attr_name)
    {
        return $this->attributes[$attr_name]->get_value();
    }

    /**
     * @param $attr_name
     */
    function attr_set_default($attr_name)
    {
        if (isset($this->attributes[$attr_name]['default'])) {
            $this->attr_set_current_value($attr_name, $this->attributes[$attr_name]['default'], false);
        }
    }

    /**
     * @param $attr_name
     * @param $value
     * @param bool $modify
     */
    function attr_set_current_value($attr_name, $value, $modify = true)
    {
        $this->attributes[$attr_name]->set_value($value, $modify);
        if ($modify)
            $this->values_modified[$attr_name] = true;
    }

    /**
     * @return array
     */
    function attr_get_all_names()
    {
        return array_keys($this->attributes);
    }

    /**
     * @param string $attr_name
     *
     * @return bool
     */
    function attr_exists($attr_name)
    {
        return isset($this->attributes[$attr_name]);
    }

    /**
     * @param $name
     * @return $this
     */
    function set_base_table($name)
    {
        $this->basetable = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    function get_base_table()
    {
        return $this->basetable;
    }

    /**
     * @param $attrs
     * @return array
     */
    function get_used_tables($attrs)
    {
        $tables = [];
        foreach ($attrs as $attr) {
            $type = $this->get_attribute_type($attr);
            if (!$this->is_used_type($type)) continue;
            $tables[$this->basetable . ($this->basetable === 'attributes' ? '_' : '_attr_') . $this->get_table_by_type($type)] = true;
        }
        return array_keys($tables);
    }

    /**
     * Возвращает ассоциативный массив, содержащий все значения всех аттрибутов
     * param mixed $entity_id  ID сущности, (integer|array)
     *
     * @todo: фильтровать до конкретного аттрибута, а не только до его таблицы
     */
    function load_all_attributes_values($entity_id)
    {
        if (empty($entity_id)) return [];

        $query = [];
        $data = [];


        /*
         * Берем не все таблицы, а только используемые в отфильтрованных аттрибутах
         */
        $attributes = is_array($this->attribute_filter) && !empty($this->attribute_filter) ? $this->attribute_filter : $this->attr_get_all_names();

        $used_attrubutes_ids = [];
        if (is_array($this->attribute_filter)) {
            foreach ($this->attribute_filter as $fname) {
                $used_attrubutes_ids[] = $this->map_get_attr_id($fname);
            }
        }

        $tables = $this->get_used_tables($attributes);
        if (!isset($tables[0])) return null;


        if (is_array($entity_id)) {
            foreach ($entity_id as &$e) {
                $e = DB::quote($e, 'int');
            }
            $idstring = ' in (' . implode(',', $entity_id) . ')';
        } else {
            $idstring = '=' . DB::quote($entity_id, 'int');
        }

        $table_names = $this->map_get_table_name();
        $table_key = $this->map_get_table_name() === 'attributes' ? 'id' : 'attr_id';

        foreach ($tables as $table_name) {
            $fields = $table_names === 'attributes' ? ($table_name . '.*, ' . $table_names . '.id as attr_id, ' . $table_names. '.name as attr_name, ' . $table_names . '.is_multiple') : '*';
            $query[] = '
                select ' . $fields . ' 
                from ' . $table_name
                . ' inner join ' . $table_names . ' on (' . $table_name . '.attr_id = ' . $table_names . '.' . $table_key . ') '
                . ' where 
                    entity_id ' . $idstring
                . ($used_attrubutes_ids ? (' and ' . $table_names . '.' . $table_key . ' in (' . implode(',', $used_attrubutes_ids) . ') ') : ''
                );
        }

        $refquery = [];
        foreach ($attributes as $attr_name) {
            $attr = $this->get_attr($attr_name);

            if ($attr->is_refbook()) {
                $table = $this->get_table_name_by_attr_type($attr->get_type());
                $refquery[] = 'select atrt.id, atrt.entity_id, attr_names.attr_name, rv.name as refbooks_value from ' . $table . ' as atrt inner join refbooks as rv on (rv.id=atrt.value) inner join ' . $this->map_get_table_name() . ' as attr_names using (attr_id) where atrt.entity_id ' . $idstring . ' and atrt.attr_id="' . $this->map_get_attr_id($attr_name) . '"';
            } elseif ($attr->is_source()) {
                $table = $this->get_table_name_by_attr_type($attr->get_type());

                /**
                 * Источник может быть внешний, в таком случае пропускаем подключение
                 */
                $source_table = $attr->source;
                $key = $attr->foreign_key;
                if ($source_table && $key) {
                    $source_name = $attr->has_variable('foreign_field') ? $attr->foreign_field : '?t.name';
                    $source_name = str_replace('?t', 'rv', $source_name);

                    $refquery[] = 'select atrt.id, atrt.entity_id, attr_names.attr_name, ' . $source_name . ' as refbooks_value from ' . $table . ' as atrt inner join ' . $source_table . ' as rv on (rv.' . $key . '=atrt.value) inner join ' . $this->map_get_table_name() . ' as attr_names using (attr_id) where atrt.entity_id ' . $idstring . ' and atrt.attr_id="' . $this->map_get_attr_id($attr_name) . '"';
                }


            }
        }

        $refdata = empty($refquery) ? [] : DB::queryDB('(' . implode(') UNION ALL (', $refquery) . ')', null, 'assoc');

        $query = '(' . implode(') UNION ALL (', $query) . ')';


        return array_merge(DB::queryDB($query, null, 'assoc'), $refdata);
    }


    function load($entity_id)
    {
        $this->revert_to_default();
        $this->set_entity_id($entity_id);
        $data = $this->load_all_attributes_values($this->entity_id);

        if (!is_array($data)) return false;

        $this->assign_attributes($data, false);

        return true;

    }

    function assign_attributes(&$data, $modify = true, $unset = false)
    {
        foreach ($data as $key => &$record) {
            if ($record['entity_id'] != $this->entity_id) continue;
            if (!$this->attr_exists($record['attr_name'])) continue;
            $attr = $this->get_attr($record['attr_name']);


            if (array_key_exists('value', $record)) {

                if ($attr->is_multiple()) {
                    $attr->add_value($record['value'], $modify)->set_id($record['id']);
                } else {
                    $attr->set_value($record['value'], $modify)->set_id($record['id']);
                }


            } elseif (array_key_exists('refbooks_value', $record)) {
                if ($attr->is_multiple()) {
                    $attr->add_human_value($record['refbooks_value']);
                } else {
                    $attr->set_human_value($record['refbooks_value']);
                }
            }
            if ($unset) unset($data[$key]);
        }
    }


    function set_entity_id($entity_id)
    {
        $this->entity_id = (integer)$entity_id;
        return $this;
    }

    function get_entity_id()
    {
        return $this->entity_id;
    }

    function set_company_id($company_id)
    {
        $this->company_id = (integer)$company_id;
        return $this;
    }

    function get_company_id()
    {
        return $this->company_id;
    }

    function get_entity_tables_names()
    {
        return self::$table_map;
    }

    function get_table_by_type($type)
    {
        return self::$table_map[$type];
    }

    function is_used_type($type)
    {
        return isset(self::$table_map[$type]);
    }

    function to_array()
    {
        $values = [];
        foreach ($this->attr_get_all_names() as $attr_name) {
            $attr = $this->get_attr($attr_name);
            if (!$attr->has_value(true)) continue;

            $values[$attr_name] = $attr->get_value();
        }
        return $values;
    }

    function to_human_array()
    {
        $values = [];
        foreach ($this->attr_get_all_names() as $attr_name) {
            $attr = $this->get_attr($attr_name);
            if (!$attr->has_value(true)) continue;

            $values[$attr_name] = $attr->get_human_value();
        }
        return $values;
    }

    function to_export_array()
    {
        $values = [];
        foreach ($this->attr_get_all_names() as $attr_name) {
            $attr = $this->get_attr($attr_name);

            if (!$this->is_used_type($attr->get_type())) continue;

            //if (!$attr->has_value(true)) continue; - возвращаем ВСЕ разрешенные к экспорту данные
            if (!$attr->allow_export()) continue;

            if ($attr_name == 'geo_complex') {
                if ($this->get_attr('estate_geo_hideComplex')->has_value()) {
                    $attr->set_value('');
                }
            }

            $values[$attr_name] = $attr->get_value();
            if (is_array($values[$attr_name])) {
                $values[$attr_name] = count($values[$attr_name]) ? implode(',', $values[$attr_name]) : NULL;
            } elseif ($values[$attr_name] === '') {
                $values[$attr_name] = NULL;
            }
            if ($attr->use_human()){
                $values[$attr_name . '_human'] = $values[$attr_name] ? $attr->get_human_value() : NULL;
            }
        }
        return $values;
    }

    /**
     * Экспорт сущности в сериализованный массив
     * применяется для хранения кэшированных данных сущности
     *
     * @return string
     *
     */
    function export_to_string()
    {
        $this->map_preload_attr_names();
        $values = [];
        foreach ($this->attr_get_all_names() as $attr_name) {
            if (!$this->map_has_attr_id($attr_name)){
                continue;
            }
            $attr = $this->get_attr($attr_name);
            if (!is_a($attr, 'SDB_Attribute_geo') && !$attr->allow_export()) {
                continue;
            }

            if (!$attr->has_value()) continue;
            if ($attr->is_refbook() || $attr->is_source()) {

                if ($attr->is_multiple()) {
                    $value = array_intersect_key($attr->get_stock_values(), array_combine($attr->get_value(), $attr->get_value()));
                } else {
                    $value = array($attr->get_value(), $attr->get_human_value());
                }


            } else {
                $value = $attr->get_value();

                if ($attr->get_type() == 'decimal') {
                    if (is_array($value)) {
                        $value = array_map('strval', $value);
                    } else {
                        $value = strval($value);
                    }

                }
            }
            $values[$this->map_get_attr_id($attr_name)] = $value;
        }

        return serialize($values);
    }

    /**
     * Восстановление сущности из закэшированных значений
     *
     * @param $string
     *
     * @return SDB_Entity
     */
    function import_from_string($string)
    {
        $data = @unserialize($string);
        $this->map_preload_attr_names();
        $names = array_flip($this->attr_names);


        if (is_array($data)) {

            foreach ($data as $attr_name => $value) {
                if (isset($names[$attr_name])) {
                    $attr_name = $names[$attr_name];
                }
                if (!$this->attr_exists($attr_name)) continue;

                $attr = $this->get_attr($attr_name);

                if ($attr->is_refbook() || $attr->is_source()) {

                    if (is_array($value)) {
                        if ($attr->is_multiple()) {

                            $attr->set_value(array_keys($value));
                            $attr->set_human_value(implode(', ', array_values($value)));

                        } elseif (isset($value[0], $value[1])) {
                            $attr->set_value($value[0]);
                            $attr->set_human_value($value[1]);
                        } else {
                            $val = array_keys($value);
                            $val_h = array_values($value);
                            $attr->set_value(reset($val));
                            $attr->set_human_value(reset($val_h));
                        }
                    } else {
                        $attr->set_value($value);
                    }

                } else {
                    $attr->set_value($value);
                }

            }
        }

        return $this;
    }

    function import_from_string_to_array($string)
    {
        $data = @unserialize($string);
        $this->map_preload_attr_names();
        $names = array_flip($this->attr_names);

        $values = [];
        if (is_array($data)) {

            foreach ($data as $attr_name => $value) {
                if (isset($names[$attr_name])) {
                    $attr_name = $names[$attr_name];
                }
                if (!$this->attr_exists($attr_name)) continue;

                $values[$attr_name] = $value;

            }
        }


        return $values;
    }


    //deprecated
    function export_all_attributs_values()
    {
        return $this->to_array();
    }


    /**
     *  Обновление значений изменившихся либо переданных аттрибутов
     *
     * @param array $new_values новые значения аттрибутов
     * @param bool  $limit_to_new deprecated
     *
     */
    function update_all_attributes_values($new_values = false, $limit_to_new = false)
    {

        $todelete = [];
        $toupdate = [];
        $toinsert = [];

        $updateOn = false;

        if (is_array($new_values)) {
            /**
             * При заданных параметрах, обновляется модель
             */
            $updateOn = true;
        } else {
            $new_values = [];
            foreach ($this->attr_get_all_names() as $attr_name) {
                $attr = $this->get_attr($attr_name);
                if (!$attr->is_modified()) {
                    continue;
                }

                $new_values[$attr_name] = $attr->get_raw_value();
            }
        }


        $mods = [];
        if (empty($new_values)) {
            return $mods;
        }
        foreach (array_keys($new_values) as $attr_name) {
            if (!$this->attr_exists($attr_name)) {
                continue;
            }
            // нельзя сохранять значения за пределами выбранного фильтра,
            // чтобы избежать внесения в базу двойного значения для не множественного аттрибута
            if (is_array($this->attribute_filter) && !in_array($attr_name, $this->attribute_filter)){
                continue;
            }

            $attr = null;
            $attr = $this->get_attr($attr_name);

            if (!$this->is_used_type($attr->get_type())) {
                continue;
            }

            $attr_old_value = $attr->get_old_value();


            /*
             * Приводим к виду, хранящемуся в БД
             */
            $new_values[$attr_name] = $attr->prepare_value($new_values[$attr_name]);

            if ($updateOn) {
                if (!$attr_old_value) {
                    $attr_old_value = $new_values[$attr_name];
                }
                $new_values[$attr_name] = $attr->set_value($new_values[$attr_name], true)->get_raw_value();
            }


            if ($attr->is_multiple()) {

                if (!is_array($new_values[$attr_name]) or $this->is_empty_value($new_values[$attr_name])) {
                    $new_values[$attr_name] = [];
                }

                $new_vals = [];
                $fix_values = $new_values[$attr_name];

                foreach ((array)$attr->get_id() as $num => $attr_num_id) {

                    $value = isset($attr_old_value[$num]) ? $attr_old_value[$num] : null;

                    if (array_key_exists($num, $new_values[$attr_name]) && !$this->is_empty_value($new_values[$attr_name][$num])) {
                        if (($attr->is_modified() and $new_values[$attr_name][$num] != $value)) {
                            $toupdate[$attr_name][] = array($attr_num_id, $new_values[$attr_name][$num]);
                        }
                        unset($new_values[$attr_name][$num]);
                    } else {
                        $todelete[$this->get_table_by_type($attr->get_type())][] = $attr_num_id;

                        $mods[] = implode(':', array('-', $this->map_get_attr_id($attr_name), $value));
                    }
                }

                if (sizeof($new_values[$attr_name])) {
                    foreach (array_keys($new_values[$attr_name]) as $num) {
                        if ($this->is_empty_value($new_values[$attr_name][$num])) {
                            continue;
                        }
                        $toinsert[$attr_name][] = $new_values[$attr_name][$num];
                        $new_vals[] = $new_values[$attr_name][$num];
                    }
                }
                if (isset($new_vals[0]) && !isset($toupdate[$attr_name])) {
                    if ($attr->allow_log()) {
                        $mods[] = implode(':', array('+', $this->map_get_attr_id($attr_name), implode(',', $new_vals)));
                    }
                }

                if (isset($toupdate[$attr_name])) {
                    if ($attr->allow_log()) {
                        $mods[] = implode(':', array('*', $this->map_get_attr_id($attr_name), implode(',', $fix_values)));
                    }
                }

                //not multiply
            } else {

                if (is_array($attr_old_value)) {
                    $attr_old_value = implode(',', (array)$attr->get_human_value());
                }
                if (is_array($new_values[$attr_name])) {
                    $new_values[$attr_name] = reset($new_values[$attr_name]);
                    if (empty($new_values[$attr_name])) $new_values[$attr_name] = null;
                }


                if ($attr->is_zero_possible()) {
                    if ($this->is_empty_value($new_values[$attr_name])) $new_values[$attr_name] = null;
                } elseif (empty($new_values[$attr_name])) {
                    $new_values[$attr_name] = null;
                }

                if ($attr->get_id()) {


                    $action = false;
                    if ($attr->is_list()) {


                        /**
                         * Ключ не в списке
                         */
                        if (!$attr->is_lists_key($new_values[$attr_name])) {
                            $action = 'delete';
                            /**
                             * Ключ в списке но значение не равно предыдущему
                             */
                        }
                        /*elseif (!$this->is_empty_value($new_values[$attr_name]) and $attr->is_modified() or $attr->get_value() != $new_values[$attr_name])){

                            $action='update';
                        }*/
                    }


                    if (!$action && $this->is_empty_value($new_values[$attr_name])) {
                        $action = 'delete';
                    }


                    if (!$action && !$this->is_empty_value($new_values[$attr_name]) and $attr->is_modified()) {
                        $action = 'update';
                    }


                    if ($action == 'delete') {

                        if ($attr->is_keep() && $attr->get_old_value()) {

                        } else {
                            $todelete[$this->get_table_by_type($attr->get_type())][] = $attr->get_id();
                            $attr->on_delete_value($attr->get_id());
                            $mods[] = implode(':', array('-', $this->map_get_attr_id($attr_name), $attr_old_value));
                        }

                    } elseif ($action == 'update') {
                        $toupdate[$attr_name][] = array($attr->get_id(), $new_values[$attr_name]);
                    }


                } else {

                    //NOTE: если значение берется из Refbooks то is_lists_key = true если key>0
                    //TODO: подумать над кэшированием и нарузкой если проверять
                    //см SDB_Attribute_refbook

                    if (($attr->is_list() && $attr->is_lists_key($new_values[$attr_name]))
                        || (!$attr->is_list() && !$this->is_empty_value($new_values[$attr_name]))
                    ) {

                        $toinsert[$attr_name][] = $new_values[$attr_name];
                    }


                }


                if (isset($toupdate[$attr_name])) {
                    if ($attr->allow_log()) {
                        $mods[] = implode(':', array('*', $this->map_get_attr_id($attr_name), $new_values[$attr_name]));
                    }
                }


                if (isset($toinsert[$attr_name])) {
                    if ($attr->allow_log()) {
                        $mods[] = implode(':', array('+', $this->map_get_attr_id($attr_name), $new_values[$attr_name]));
                    }
                }


            }


            if (isset($toinsert[$attr_name]) && $attr->get_type() == 'decimal') {
                $toinsert[$attr_name] = array_map(array('SDB_Entity', 'comma_to_dot'), $toinsert[$attr_name]);
            }
            if (isset($toupdate[$attr_name]) && $attr->get_type() == 'decimal') {
                $toupdate[$attr_name] = array_map(array('SDB_Entity', 'comma_to_dot'), $toupdate[$attr_name]);
            }

        }

        /**
         * Подчищаем неиспользующиеся множественные записи, хранящиеся в базе, у аттрибута с единственным значением
         */
        foreach ($this->attr_get_all_names() as $attr_name) {
            $attr = $this->get_attr($attr_name);
            if (!$attr->unused_ids) continue;
            $tablename = $this->get_table_by_type($attr->get_type());
            foreach ($attr->unused_ids as $unid) {
                $todelete[$tablename][] = $unid;
            }
        }


        $this->debug($toupdate, 'upd');
        $this->debug($todelete, 'del');
        $this->debug($toinsert, 'ins');


        if (!empty($todelete)) {

            foreach ($todelete as $table_type => $records) {
                $table = $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $table_type;
                DB::queryDB("delete from ?t where id in (?ai)", array($table, $records), 'ar');
            }
        }

        if (!empty($toupdate)) {
            foreach ($toupdate as $attr_name => $records) {

                $table = $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $this->get_table_by_type($this->get_attr($attr_name)->get_type());
                foreach ($records as $record) {
                    DB::queryDB("update ?t set value=? where id=?i", array($table, $record[1], $record[0]), 'ar');
                }
            }
        }


        if (!empty($toinsert)) {
            $entity_id = $this->entity_id;
            foreach ($toinsert as $attr_name => $records) {

                $table_type = $this->get_table_by_type($this->get_attr($attr_name)->get_type());

                $table = $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $table_type;
                $attr_id = $this->map_get_attr_id($attr_name);

                $data = [];
                foreach ($records as $value) {
                    $data[] = array($entity_id, $attr_id, $value);
                }


                DB::queryDB("insert into ?t (entity_id,attr_id,value) values ?v", array($table, $data), 'id');

            }
        }

        return $mods;

    }

    static function comma_to_dot($value)
    {
        return str_replace(',', '.', $value);
    }

    /**
     * @param int $entity_id
     *
     * @return bool
     * @deprecated
     */
    function delete($entity_id = 0)
    {
        throw new SDB_Exception('Direct deletion is prohibited. Use key constraints!');
    }

    /**
     *  Проверка существенности значения (для сохранения в базе)
     */
    function is_empty_value($value)
    {
        return is_null($value) or $value === '';
    }

    /**
     * @param $name
     *
     * @return SDB_Entity
     */
    static function factory($name)
    {
        return new SDB_Entity($name);
    }


    function add_attributes(array $attributes = [])
    {
        foreach ($attributes as $key => $record) {
            if (!isset($record['postfix']))
                $record['postfix'] = '';
            $this->attributes[$key] = $record;
            $this->values[$key] = NULL;
        }
    }

    /**
     * @return SDB_Attribute[]
     */
    function &get_attributes()
    {
        return $this->attributes;
    }

    /**
     * @param $attr_name
     *
     * @return SDB_Attribute
     */
    function &get_attr($attr_name)
    {
        if (!isset($this->attributes[$attr_name])) throw new SDB_Exception('No such attr "' . $attr_name . '"');
        return $this->attributes[$attr_name];
    }


    function attr_get_value($attr_name, $null = NULL)
    {
        if (is_null($this->attributes[$attr_name]['value']))
            return $null;
        return $this->attributes[$attr_name]['value'];
    }


    function get_attribute_type($key)
    {


        return $this->get_attr($key)->get_type();
    }


    function attribute_search($entity_id, $type, $value)
    {

        $table_name = $this->get_table_name_by_attr_type($type);

        if (is_array($value)) {

            $idstring = ' in (?a)';
        } else {
            $idstring = ' = ?';
        }


        return DB::queryDB('select value,attr_names.attr_name from ?t inner join ?t using (attr_id) where entity_id=?i and value' . $idstring, array($table_name, $this->map_get_table_name(), $entity_id, $value), 'vars');

    }

    function get_table_name_by_attr_type($type)
    {
        return $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $this->get_table_by_type($type);
    }

    function get_table_name_by_attr_name($name)
    {
        return $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $this->get_table_by_type($this->get_attr($name)->get_type());
    }


    function collect_attributes(array $request)
    {
        foreach (array_keys($request) as $key) {
            $this->set_option($key, $request[$key]);
        }
    }

    function set_option($key, $value)
    {


        if ($this->attr_exists($key)) {
            $attr = $this->get_attr($key);

            if ($attr->is_multiple() && $this->is_empty_value($value)) {
                $value = [];
            }

            if ($attr->is_list() && !$attr->is_source() && !$attr->is_refbook()) {


                $values = $attr->get_stock_values();


                if ($attr->is_multiple()) {
                    if (!is_array($value)) $value = array($value);

                    $value = array_intersect($value, array_keys($values));

                    $attr->set_value($value);

                } else {
                    if (is_array($value)){
                        $value = array_shift($value);
                    }
                    if (is_bool($value)) {
                        $value = $value ? 1 : 0;
                    }
                    if (is_scalar($value)){
                        if (array_key_exists($value, $values)) {
                            //got existst key
                            $attr->set_value($value);
                        } else {
                            //get exists key's value
                            $pos = array_search($value, $values);
                            if ($pos !== false) {
                                $attr->set_value($pos);
                            } elseif ($this->is_empty_value($value)) {
                                $attr->set_value(null);
                            }
                        }
                    }
                }
            } else {
                $attr->set_value($value);
            }
        }
        return $this;
    }

    function set_category($category)
    {
        $this->category = $category;

        $this->get_attr('estate_category')->set_value($this->get_attr('estate_category')->get_id_by_url($category),false);


    }

    function get_category()
    {

        if (!empty($this->category)) {
            return $this->category;
        }
        if ($this->get_attr('estate_category')->has_value()) {

            return $this->get_attr('estate_category')->get_url_by_id($this->get_attr('estate_category')->get_value());
        }
    }


    function revert_to_default()
    {
        foreach (array_keys($this->attributes) as $attr_name) {
            $this->attributes[$attr_name]->set_default();
        }
        return $this;
    }


    function load_entity_attr_values($entity_ids, $attr_name)
    {
        $attr = $this->get_attr($attr_name);
        $table = $this->get_table_name_by_attr_type($attr->get_type());
        $attr_id = $this->map_get_attr_id($attr_name);
        return DB::queryDB("select entity_id, group_concat(value separator ';') from ?t where entity_id in (?ai) and attr_id=?i group by entity_id", array($table, $entity_ids, $attr_id), 'vars');
    }


    function map_get_attr_names()
    {
        $this->map_preload_attr_names();
        return $this->attr_names;
    }

    static private $attr_names_cache;

    function map_preload_attr_names()
    {
        if (empty($this->attr_names)) {
            if (empty(self::$attr_names_cache[$this->get_base_table()])) {

                $table = $this->get_base_table() . '_attr_names';
                $fields = [
                    'attr_name',
                    'attr_id'
                ];

                $conditions = [];
                $data = [
                    'table' => $table
                ];

                if ($this->get_base_table() === 'attributes') {

                    $table = $this->get_base_table();
                    $fields = [
                        'name as attr_name',
                        'id as attr_id'
                    ];

                    $conditions = [
                        'company_id = ?i:company_id',
                        'entity = ?:entity',
                        'is_hidden = 0',
                        'is_deleted = 0'
                    ];
                    $data = [
                        'table' => $table,
                        'company_id' => $this->get_company_id(),
                        'entity' => $this->namespace
                    ];
                }

                self::$attr_names_cache[$this->get_base_table()] = DB::queryDB(
                    "
                        select SQL_CACHE " . implode(', ', $fields) . " 
                        from ?t:table 
                        " . ($conditions ? ' where ' . implode(' and ', $conditions) : '') . "
                        order by null
                    ",
                    $data,
                    'vars'
                );
            }
            $this->attr_names = self::$attr_names_cache[$this->get_base_table()];
        }
    }

    /**
     * @param $attr_name
     * @return mixed
     * @throws SDB_Exception
     */
    function map_get_attr_id($attr_name)
    {
        $this->map_preload_attr_names();
        if (!$this->map_has_attr_id($attr_name)) {
            throw new SDB_Exception($attr_name . ' not found, may be DB Schema not synchronized?');
        }
        return $this->attr_names[$attr_name];
    }

    /**
     * @param $attr_name
     * @return bool
     */
    function map_has_attr_id($attr_name){
        $this->map_preload_attr_names();
        return isset($this->attr_names[$attr_name]);
    }


    function map_update_attr_names()
    {

        $this->map_preload_attr_names();


        $new_names = [];
        foreach ($this->attr_get_all_names() as $attr_name) {
            $attr = $this->get_attr($attr_name);
            if (!$this->is_used_type($attr->get_type())) continue;
            if (isset($this->attr_names[$attr_name])) continue;
            $new_names[] = array($attr_name);
        }
        if (!empty($new_names)) {
            DB::queryDB("insert IGNORE into ?t (attr_name) values ?v", array($this->get_base_table() . '_attr_names', $new_names));
            $this->attr_names = null;
            $this->map_preload_attr_names();
        }

    }

    function map_get_table_name()
    {
        return $this->get_base_table() === 'attributes' ? $this->get_base_table() : $this->get_base_table() . '_attr_names';
    }

    /**
     * формирует FOREIGN KEYs для таблиц аттрибутов
     */
    function db_update_constraints()
    {
        DB::queryDB("SET FOREIGN_KEY_CHECKS=0");

        $tables = $this->get_used_tables(array_keys($this->get_attributes()));

        $keys = DB::queryDB("SELECT TABLE_NAME,GROUP_CONCAT(CONSTRAINT_NAME SEPARATOR ',') FROM information_schema.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = DATABASE() and CONSTRAINT_TYPE='FOREIGN KEY' and TABLE_NAME in (?a) GROUP BY TABLE_NAME", array($tables), 'vars');


        foreach ($tables as $table) {

            //Удаляем ВСЕ внешние ключи
            if (isset($keys[$table])) {
                foreach (explode(',', $keys[$table]) as $fkey) {
                    DB::queryDB("ALTER TABLE ?t DROP FOREIGN KEY ?c", array($table, $fkey));
                }
            }

            $e_k_name = $table . '_entity_id';
            $e_a_name = $table . '_attr_id';


            DB::queryDB("ALTER TABLE ?t ADD CONSTRAINT ?c FOREIGN KEY (?c) REFERENCES ?t (?c) ON DELETE CASCADE ON UPDATE CASCADE;", array($table, $e_k_name, 'entity_id', $this->get_base_table(), 'id'));

            DB::queryDB("ALTER TABLE ?t ADD CONSTRAINT ?c FOREIGN KEY (?c) REFERENCES ?t (?c) ON DELETE RESTRICT ON UPDATE CASCADE;", array($table, $e_a_name, 'attr_id', $this->map_get_table_name(), 'attr_id'));
        }

        DB::queryDB("SET FOREIGN_KEY_CHECKS=1");

    }

    function db_clone($old_entity_id, $new_entity_id)
    {
        $tables = array_unique(array_values(self::$table_map));
        foreach ($tables as $table) {
            $table = $this->basetable . ($this->basetable === 'attributes' ? '_' :'_attr_') . $table;

            DB::queryDB("insert into ?t (entity_id,attr_id,value) select ?i,attr_id,value from ?t t2 where t2.entity_id=?i order by t2.id", array($table, $new_entity_id, $table, $old_entity_id));
        }
    }


    /**
     * @param $callback
     *
     * @return null;
     */
    function set_debug($callback)
    {
        if (is_callable($callback, true)) {
            $this->debug_func = $callback;
        }

    }

    /**
     * @param $data
     *
     * @return null
     */
    function debug($data)
    {
        if ($this->debug_func && sizeof(($arg_list = func_get_args()))) {
            call_user_func_array($this->debug_func, $arg_list);
        }
    }


}