<?php


class SDB_Entity_commestate extends SDB_Entity {

    protected $basetable='estate';

     function __construct($name='estate:comm', $filter=null) {
        parent::__construct($name,$filter);
    }

    /**
     * Оставлено для совместимости
     * todo: заменить все вызовы на helpers_estate::create_title
     * @param string $options
     * @param string $category
     * @param string $category_name
     * @return string
     * @throws Exception
     */
    function create_title($options='',$category='comm',$category_name=''){

        if (!is_array($options)) {
            $options=array('activity'=>$options);
        }

        return helpers_estate::create_title($this,'comm',null,$options);
    }

    protected $replaces=array(
        'Торговые помещения'=>'Торг.помещ.'
    );
    function create_short_title($options='',$category='',$category_name=''){
        $str=$this->create_title($options,$category,$category_name);

        $str=str_replace(array_keys($this->replaces),array_values($this->replaces),$str);
        return $str;
    }



    function check_valid_raw_data(){
        $errors=array();

        return $errors;
    }

}