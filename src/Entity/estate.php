<?php


class SDB_Entity_estate extends SDB_Entity {

    protected $basetable='estate';

    /**
     * @param $type
     * @param null $filter
     * @return SDB_Entity_estate | SDB_Entity_commestate
     */
    static function factory($type,$filter=null){

        $classname='SDB_Entity_'.($type=='living' ? '' : $type).'estate';
        return new $classname('estate:'.$type,$filter);
    }

    function __construct($name='estate:living', $filter=null) {

        parent::__construct($name,$filter);
    }

	/**
     * Оставлено для совместимости
     * todo: заменить все вызовы на helpers_estate::create_title
     * @param string $options
     * @param string $category
     * @param string $category_name
     * @return string
     * @throws Exception
     */
    function create_title($options='',$category='living',$category_name=''){

        if (!is_array($options)) {
            $options=array('activity'=>$options);
        }

        return helpers_estate::create_title($this,'living',null,$options);
    }

    function get_short_category_name($name){

        return str_replace(array('Квартира','Дом / коттедж','Парковка / гараж','Земельный участок'),array('Кв.','Дом/котт.','Гараж/парк.','Зем.уч.'),$name);
    }


    function create_short_title($options='',$category='',$category_name=''){

        $title=array();

        $show_hidden=true;

        if (is_array($options)){
            extract($options);
        } else {
            $activity=$options;
        }
        if (empty($activity)){
            $activity=$this->get_attr('estate_activity')->get_human_value();
        }
        $activity=str_replace(array('Продается','Куплю'),array('Прод.','Куп.'),$activity);
        $title[]=$activity;

        $activity=$this->get_attr('estate_activity')->get_value();


        if (empty($category)){
            $category= $this->get_attr('estate_category')->get_value();
        }
        if (empty($category_name)){
            $category_name= $this->get_attr('estate_category')->get_human_value();
        }


        if ($category=='flat'){
            $attr=$this->get_attr('estate_rooms');
            if ($attr->has_value()){
                $title[]=$attr->get_human_value(false).'-к';
            }
        }

        $actkey=Estate::merge_activity($this->get_attr('estate_activity')->get_value());

        if ($category_name==''){
            $category_name = $this->get_attr('estate_category')->get_field_by_url('name',$category);
        }
        $category_name=$this->get_short_category_name($category_name);


        $attr2=$this->get_attr('estate_category_type');
        if ($category!=''){
            switch ($category){



                case 'flat':
                    $type=$attr2->get_human_value();
                    if ($this->get_attr('estate_living_new')->get_value()==1){
                        $type='';
                    }
                    $title[]=$category_name.($this->get_attr('estate_living_new')->get_value()==1 ? ' в нов.':'').($type=='' ? '' : '('.$type.')');

                    break;
                //дома-коттеджи
                case 'house':
                case 'garage':

                    $title[]=$attr2->has_value() ? $attr2->get_human_value() : $category_name;
                    break;

                case 'land':
                    $title[]=$category_name;
                    break;

                default:
                    $title[]=$category_name.($attr2->has_value() ? ' ('.$attr2->get_human_value().')':'');
                    break;
            }


            $attr=$this->get_attr('geo_complex');
            if ($attr->get_value()){
               $title[]='ЖК "'.$attr->get_human_value().'"';
            }

            $title=array(implode(' ',$title));

            $attr=$this->get_attr('geo_complex_set');
            if ($attr->has_value()){
                $complex=$this->get_attr('geo_complex_set')->get_human_value();
                if (!empty($complex)){
                    $title[count($title)-1].=' в ЖК: '.$complex.'';
                }
            }
        }



        if ($activity=='buy'){
            $title[]=$this->get_attr('geo')->get_short_title('%geo_city%, %geo_street%');

        } else {
            $title[]=$this->get_attr('geo')->get_short_title('%geo_city%, %geo_street%, %geo_house%, %geo_flatnum%',$show_hidden);
        }





        if ($activity=='buy'){
            $districts=$this->get_attr('geo_district_set')->get_human_value();
            if (!empty($districts)){
                $title[]='р-ны: "'.(is_array($districts) ? implode(', ',$districts) : $districts).'"';
            }
        }

        $attr=$this->get_attr('estate_floor');
        if ($attr->get_value()){
            $title[]='эт: '.$attr->get_human_value();
        }


        $attr=$this->get_attr('estate_area_range');
        if ($attr->has_value()){
            $title[]=$attr->get_human_value().$attr->get_suffix();
        }


        $attr=$this->get_attr('estate_area');

        if ($attr->has_value()){
            $title[]=$attr->get_value().$attr->get_suffix();
        }

        $attr=$this->get_attr('estate_price_range');

        if ($attr->has_value()){
            $title[]=$attr->get_human_value().$attr->get_suffix();
        }

        $attr=$this->get_attr('estate_price');
        if ($attr->has_value()){
            $title[]=$attr->get_human_value();
        }



        return implode(', ',$title);


	}
	

	
	function check_valid_raw_data(){
		$errors=array();
		$estate_floor=$this->get_attr('estate_floor');
		$estate_floors_in_house=$this->get_attr('estate_floors_in_house');
		
		if ($estate_floor->has_value() && $estate_floors_in_house->has_value() && $estate_floor->get_value() > $estate_floors_in_house->get_value()){
			$errors[]=__text('estate:Number of floors is greater than floors in house');
		}




       /* if ($this->get_attr('estate_price')->get_value()>$this->get_attr('estate_price')->get_maxvalue()){
            $errors[]=__text('estate:Price is to too much (maximum: %%max%%)',array('max'=>$this->get_attr('estate_price')->get_maxvalue()));
        }*/
		return $errors;
	}
	
}