<?php


class SDB_Entity_realestate extends SDB_Entity {

    protected $basetable='estate';

     function __construct($name='estate:comm', $filter=null) {
        parent::__construct($name,$filter);
    }

    function create_title($options='',$category='real',$category_name=''){
        $title='';


        $show_hidden = false;

        if (is_array($options)) {
            extract($options);
        } else {
            $activity = $options;
        }


        $title[]=$this->get_attr('estate_activity')->get_human_value();


        $activity=$this->get_attr('estate_activity')->get_value();
        $activity_plus=$this->get_attr('estate_activityPlus')->get_value();






        $type=$this->get_attr('estate_category_type');
        $subtype=$this->get_attr('estate_category_subtype');



        if ($subtype->has_value()){
            $title[]=$subtype->get_human_value();
        } else {
            $title[]=$type->get_human_value();
        }


            $attr=$this->get_attr('estate_area_range');
            if ($attr->has_value()){
                $title[]=$attr->get_human_value().' '.$attr->get_suffix();
            }



            $attr=$this->get_attr('estate_area');

            if ($attr->has_value()){
                $title[]=$attr->get_human_value().' '.$attr->get_suffix();
            }





        $attr=$this->get_attr('estate_price_range');


        if ($attr->has_value()){

            $title[]=$attr->get_human_value().' '.$attr->get_suffix();
        }

        if ($activity_plus!='' or in_array($activity,array('sell','buy'))){
            $attr=$this->get_attr('estate_price');
            if ($attr->has_value()){
                $title[]='цена: '.$attr->get_human_value();
            }
        }







        if (Estate::merge_activity($activity)=='buy'){

            if ($activity=='rent' or $activity_plus=='rent' or $activity=='lease' or $activity_plus=='lease'){
                $attr=$this->get_attr('estate_price_rent_m2_range');
                if ($attr->has_value()){
                    $title[]='аренда за кв.м: '.$attr->get_human_value().' '.$attr->get_suffix();
                }
            }

            $title[]=$this->get_attr('geo')->get_short_title('%geo_city%, %geo_street%,%geo_house%');

            $districts=$this->get_attr('geo_district_set')->get_human_value();

            if (!empty($districts)){

                $title[]='районы: "'.$districts.'"';
            }



        } else {

            if ($activity=='rent' or $activity_plus=='rent' or $activity=='lease' or $activity_plus=='lease'){
                $attr=$this->get_attr('estate_price_rent_m2');
                if ($attr->has_value()){
                    $title[]='аренда за кв.м: '.$attr->get_human_value();
                } else {
                    $attr=$this->get_attr('estate_price_rent');
                    if ($attr->has_value()){
                        $title[]='цена за все: '.$attr->get_human_value();
                    }
                }
            }



            $title[] = $this->get_attr('geo')->get_short_title(SDB_Attribute_geo::$geo_template_default, $show_hidden);
        }


        $attr=$this->get_attr('estate_floor');
        if ($attr->has_value()){
            $title[]='этаж: '.$attr->get_human_value();
        }






        return implode(', ',$title);


    }



    function check_valid_raw_data(){
        $errors=array();

        return $errors;
    }

}