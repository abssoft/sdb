<?php

/**
 * Used to draw form element for attributes
 *
 * @author Артем
 */
class SDB_Form {
  static function draw_status_yes(){
    return '<span class="SDB_status_yes">&radic;</span>';
  }

  static function draw_status_no(){
    return '<span class="SDB_status_no">&times;</span>';
  }

  static function draw_status_yes_no($status){
    return $status ? self::draw_status_yes():self::draw_status_no();
  }
}

?>
